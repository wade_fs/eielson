//-----------------------------------------------------------------------------
// UART_MultiUART.c
//-----------------------------------------------------------------------------
// Copyright 2014 Silicon Laboratories, Inc.
// http://developer.silabs.com/legal/version/v11/Silicon_Labs_Software_License_Agreement.txt
//
// Program Description:
//
// This program configures UART0 and UART1 to operate in polled mode, suitable
// for use with the stdio.h functions printf() and scanf().
//
// Test code is included for printf() and getchar(), and shows an example
// of how the putchar() and _getkey() library routines can be overridden to
// allow for multiple UARTs to be selected by the functions (in this example, 
// the global variable UART is used to select between UART0 and UART1).
//
// The example sets system clock to maximum frequency of 48 MHz.
//
//
// How To Test:
//
//
// 1) Place the switch in "AEM" mode.
// 2) For UART1, Make the following connections from the EFM8UB1 STK to a
//    CP210x-EK:
//    EFM8UB2 P0.6 (TX) - CP210x RX
//    EFM8UB2 P0.7 (RX) - CP210x TX
//    EFM8UB2 GND       - CP210x GND
// 3) Connect the EFM8UB2 STK board to a PC using a mini USB cable.
// 4) Connect the CP210x-EK board to the PC using a USB cable.
// 5) Compile and download code to the EFM8UB2 STK board.
//    In Simplicity Studio IDE, select Run -> Debug from the menu bar,
//    click the Debug button in the quick menu, or press F11.
// 6) For UART0: On the PC, open HyperTerminal (or any other terminal program)
//    and connect to the JLink CDC UART Port at 115200 baud rate and 8-N-1.
// 7) For UART1: On the PC, open HyperTerminal (or any other terminal program)
//    and connect to the CP210x virtual COM port at 115200 baud rate and 8-N-1.
// 8) Run the code.
//    In Simplicity Studio IDE, select Run -> Resume from the menu bar,
//    click the Resume button in the quick menu, or press F8.
// 9) Upon reset, each UART will print out a hello message. Additionally,
//    sending a '1' or '0' to the appropriately selected UART (chosen on
//    line 127) will turn the LED on and off.
//
//
// Target:         EFM8UB2
// Tool chain:     Simplicity Studio / Keil C51 9.51
// Command Line:   None
//
// Release 1.0 (BL)
//    - Initial Release
//    - 14 JAN 2015
//
//
//
//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Global CONSTANTS
//-----------------------------------------------------------------------------
// History 
// 2017.01.10 version 0.1
#include "app.h"
#include "APP001_LCD.h"
#include "def.h"
#include "mcc_generated_files/eusart1.h"


#define   FOR_TEST 1
#define  PC_BUF_DEBUG    1
#define  GET_CHECKSUM_PC 1
#define  TEST_MULTI 1
#define  USE_SLEEP 1

#ifdef TEST_MULTI 
unsigned int  DataSize=640;
//unsigned int  DataSize=330;
unsigned int  MultiFlag=1;
#else
unsigned int  DataSize=0;
unsigned int  MultiFlag=0;
#endif

unsigned char lcdOnFlag=0;
unsigned int  LCD_index=0;
unsigned int  times16;

unsigned char FinishFlag=0;
unsigned char DisplayFlag=0;
unsigned int rx_bytes=0;
unsigned int rx_index=0;
unsigned char rs232_s=0;
unsigned char rs232_FC=0;
unsigned char wait_bytes=0;
void store_data (unsigned char cc);
void log_data(unsigned char cc);
void showTitle(void);
unsigned char putdata (unsigned char c);
void display (void);
void checksum (void);
unsigned char decode ( unsigned char cc);
unsigned char encode ( unsigned char cc);
void sendDataOut (unsigned int start , unsigned int ending , unsigned char checksuml ,unsigned char checksumh );
void sendHeadOut (unsigned int DataSS,unsigned int DataS,unsigned char flag);
unsigned int GetDataSize (void);
//unsigned int  get_real_data_size(unsigned int data);
unsigned int realDataSize=0;
void step01(void);
void step02(unsigned char cc);
void step03(void);
void step04(void);
void step05(void);
void step06(void);
void step07(void);
void step08(void);
void step09(void);
void step10(void);
void step11(void);
void step12(void);
void step13(void);
void step14(void);
void step18(void);
void switchdefault(unsigned char cc);

unsigned int i,j;

unsigned int idle=0;
unsigned int idle2=0;
unsigned char findFC=0;
unsigned char findB5=0;

bit recFlag;
unsigned char getData;

void button(void);

extern void putrs1USART_2(unsigned char data);
bit UART = 0;                          // '0 is UART0; '1' is UART1

unsigned char Header[]={ 0x80, 0x80, 0x0A }; // 10 Bytes header
unsigned char Header2[]={ 0x00, 0x81, 0x0A }; // 10 Bytes header
unsigned char Header3[]={ 0x80, 0x00, 0x0A }; // 10 Bytes header

unsigned char UART_Buffer[UART_BUFFER];

unsigned char lcd_size=0;

unsigned int pc_buf_index=0;
unsigned int lcd_buf_index=0;

unsigned char lcd_log_flag=0;

unsigned char PC_h1_index=0;
unsigned char PC_h2_index=0;
unsigned char PC_h3_index=0;
unsigned char PC_h4_index=0;
unsigned char PC_H1[HEADER_BUFFER];
unsigned char PC_H2[HEADER_BUFFER];
unsigned char PC_H3[HEADER_BUFFER];
unsigned char PC_H4[HEADER_BUFFER];

#ifdef PC_BUF_DEBUG
unsigned char PC_BUF[MAX_PC_ARRAY];
#endif
unsigned char LCD_BUF0[]={
// 1      2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 1
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 3
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 5
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 6
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 7 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 8
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 9
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 10
  0x00,

// 1      2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20
  0x84, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
  0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x0B, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

// 1      2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20
  0x00, 0xD4, 0xA0, 0x00, 0x90, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x00,

 ///////////// ENd     
  // 1      2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20
		 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		 0x00,00
  
};
unsigned char LCD_BUF1[]={
// 1      2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20
  0x84, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 
  0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x57, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xA2, 0xA0, 0x00, 0x90, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x84, 0x00, 
  0xC4, 0xFE, 0xC8, 0x80, 0x00    // 0x0B 0xD4
 // 0x44, 0x92, 0x3B, 0x00, 0x00
 // 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x00,
 // 0x0A, 0x00, 0x00, 0x44, 0x03, 0x84, 0x00, 0xC4, 0xFE, 0xAF, 0x80, 0xB5,   // 0x0B 0xD4
//0x0A, 0x00, 0x00, 0x00, 0x00, 0x84, 0x00, 0xC4, 0xFE, 0xC8, 0x80, 0x00,   // 0x0B 0xD4
//  00 00 00 00 84 00 C4 FE C8 80 640
 //  00 00 00 00 84 00 84 07 4E 80 225
 //  00 00 00 00 84 00 84 C7 AE 80 228
};


unsigned char CHECKSUM_BUF[16]={
    0xC1,0x06,
    0xBC,0xDA,
    0x1F,0xAC,
};
  unsigned char LCD_BUF[]={
   
// 1      2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20
 // 0xCA, 0x60, 0x80, 0x80, 0x0A, 
  0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6, 0x0E, 0x8E, 0x4E, 0xCE, 0x2E, // 1
  0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, // 2
  0xF2, 0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0x86, 0x86, 0x46, 0x46, 0xC6, 0xC6, 0x26, 0x26, // 3
  0xA6, 0xA6, 0x66, 0x66, 0xE6, 0xE6, 0x16, 0x16, 0x96, 0x96, 0x56, 0x56, 0xD6, 0xD6, 0x36, 0x36, 0xB6, 0xB6, 0x76, 0x76, // 4
  0xF6, 0xF6, 0x0E, 0x0E, 0x8E, 0x8E, 0x4E, 0x4E, 0xCE, 0xCE, 0x2E, 0x2E, 0xAE, 0xAE, 0x6E, 0x6E, 0xEE, 0xEE, 0x1E, 0x1E, // 5
  0x9E, 0x9E, 0x5E, 0x5E, 0x82, 0x82, 0x42, 0x42, 0xC2, 0xC2, 0x22, 0x22, 0xA2, 0xA2, 0x62, 0x62, 0xE2, 0xE2, 0x12, 0x12, // 6
  0x92, 0x92, 0x52, 0x52, 0xD2, 0xD2, 0x32, 0x32, 0xB2, 0xB2, 0x72, 0x72, 0xF2, 0xF2, 0x0A, 0x0A, 0x8A, 0x8A, 0x4A, 0x4A, // 7
  0xCA, 0xCA, 0x2A, 0x2A, 0xAA, 0xAA, 0x6A, 0x6A, 0xEA, 0xEA, 0x1A, 0x1A, 0x9A, 0x9A, 0x5A, 0x5A, 0x86, 0x86, 0x86, 0x46, // 8
  0x46, 0x46, 0xC6, 0xC6, 0xC6, 0x26, 0x26, 0x26, 0xA6, 0xA6, 0xA6, 0x66, 0x66, 0x66, 0xE6, 0xE6, 0xE6, 0x16, 0x16, 0x16, // 9
  0x96, 0x96, 0x96, 0x56, 0x56, 0x56, 0xD6, 0xD6, 0xD6, 0x36, 0x36, 0x36, 0xB6, 0xB6, 0xB6, 0x76, 0x76, 0x76, 0xF6, 0xF6, // 10
  0xF6, 0x0E, 0x0E, 0x0E, 0x8E, 0x8E, 0x8E, 0x4E, 0x4E, 0x4E, 0xCE, 0xCE, 0xCE, 0x2E, 0x2E, 0x2E, 0xAE, 0xAE, 0xAE, 0x6E, // 11
  0x6E, 0x6E, 0xEE, 0xEE, 0xEE, 0x1E, 0x1E, 0x1E, 0x9E, 0x9E, 0x9E, 0x5E, 0x5E, 0x5E, 0x82, 0x82, 0x82, 0x42, 0x42, 0x42, // 12
  0xC2, 0xC2, 0xC2, 0x22, 0x22, 0x22, 0xA2, 0xA2, 0xA2, 0x62, 0x62, 0x62, 0xE2, 0xE2, 0xE2, 0x12, 
  //0xC1,0x06,
  //0x80, 0x80, 0x0A, 
  0x12, 0x12, 0x92, 0x92, 0x92, 0x52, 0x52, 0x52, 0xD2, 0xD2, 0xD2, 0x32, 0x32, 0x32, 0xB2, 0xB2, 0xB2, 0x72, 0x72, // 14
  0x72, 0xF2, 0xF2, 0xF2, 0x0A, 0x0A, 0x0A, 0x8A, 0x8A, 0x8A, 0x4A, 0x4A, 0x4A, 0xCA, 0xCA, 0xCA, 0x2A, 0x2A, 0x2A, 0xAA, // 15
  0xAA, 0xAA, 0x6A, 0x6A, 0x6A, 0xEA, 0xEA, 0xEA, 0x1A, 0x1A, 0x1A, 0x9A, 0x9A, 0x9A, 0x5A, 0x5A, 0x5A, 0x86, 0x86, 0x86, // 16
  0x86, 0x46, 0x46, 0x46, 0x46, 0xC6, 0xC6, 0xC6, 0xC6, 0x26, 0x26, 0x26, 0x26, 0xA6, 0xA6, 0xA6, 0xA6, 0x66, 0x66, 0x66, // 17
  0x66, 0xE6, 0xE6, 0xE6, 0xE6, 0x16, 0x16, 0x16, 0x16, 0x96, 0x96, 0x96, 0x96, 0x56, 0x56, 0x56, 0x56, 0xD6, 0xD6, 0xD6, // 18
  0xD6, 0x36, 0x36, 0x36, 0x36, 0xB6, 0xB6, 0xB6, 0xB6, 0x76, 0x76, 0x76, 0x76, 0xF6, 0xF6, 0xF6, 0xF6, 0x0E, 0x0E, 0x0E, // 19
  0x0E, 0x8E, 0x8E, 0x8E, 0x8E, 0x4E, 0x4E, 0x4E, 0x4E, 0xCE, 0xCE, 0xCE, 0xCE, 0x2E, 0x2E, 0x2E, 0x2E, 0xAE, 0xAE, 0xAE, // 20
  0xAE, 0x6E, 0x6E, 0x6E, 0x6E, 0xEE, 0xEE, 0xEE, 0xEE, 0x1E, 0x1E, 0x1E, 0x1E, 0x9E, 0x9E, 0x9E, 0x9E, 0x5E, 0x5E, 0x5E, // 21
  0x5E, 0x82, 0x82, 0x82, 0x82, 0x42, 0x42, 0x42, 0x42, 0xC2, 0xC2, 0xC2, 0xC2, 0x22, 0x22, 0x22, 0x22, 0xA2, 0xA2, 0xA2, // 22
  0xA2, 0x62, 0x62, 0x62, 0x62, 0xE2, 0xE2, 0xE2, 0xE2, 0x12, 0x12, 0x12, 0x12, 0x92, 0x92, 0x92, 0x92, 0x52, 0x52, 0x52, // 23
  0x52, 0xD2, 0xD2, 0xD2, 0xD2, 0x32, 0x32, 0x32, 0x32, 0xB2, 0xB2, 0xB2, 0xB2, 0x72, 0x72, 0x72, 0x72, 0xF2, 0xF2, 0xF2, // 24
  0xF2, 0x0A, 0x0A, 0x0A, 0x0A, 0x8A, 0x8A, 0x8A, 0x8A, 0x4A, 0x4A, 0x4A, 0x4A, 0xCA, 0xCA, 0xCA, 0xCA, 0x2A, 0x2A, 0x2A, // 25
  0x2A, 0xAA, 0xAA, 0xAA, 0xAA, 0x6A, 0x6A, 0x6A, 0x6A, 0xEA, 0xEA, 0xEA, 0xEA, 0x1A, 0x1A, 0x1A, 0x1A,
  //0xBC, 0xDA,
  //0x00,  0x81, 0x0A,
  0x9A, 0x9A, 0x9A, 0x9A, 0x5A, 0x5A, 0x5A, 0x5A, 0x86, 0x86, 0x86, 0x86, 0x86, 0x46, 0x46, 0x46, 0x46, 0x46, // 27
  0xC6, 0xC6, 0xC6, 0xC6, 0xC6, 0x26, 0x26, 0x26, 0x26, 0x26, 0xA6, 0xA6, 0xA6, 0xA6, 0xA6, 0x66, 0x66, 0x66, 0x66, 0x66, // 28
  0xE6, 0xE6, 0xE6, 0xE6, 0xE6, 0x16, 0x16, 0x16, 0x16, 0x16, 0x96, 0x96, 0x96, 0x96, 0x96, 0x56, 0x56, 0x56, 0x56, 0x56, // 29
  0xD6, 0xD6, 0xD6, 0xD6, 0xD6, 0x36, 0x36, 0x36, 0x36, 0x36, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0x76, 0x76, 0x76, 0x76, 0x76, // 30
  0xF6, 0xF6, 0xF6, 0xF6, 0xF6, 0x0E, 0x0E, 0x0E, 0x0E, 0x0E, 0x8E, 0x8E, 0x8E, 0x8E, 0x8E, 0x4E, 0x4E, 0x4E, 0x4E, 0x4E, // 31
  0xCE, 0xCE, 0xCE, 0xCE, 0xCE, 0x2E, 0x2E, 0x2E, 0x2E, 0x2E, 0xAE, 0xAE, 0xAE, 0xAE, 0xAE, 0x6E, 0x6E, 0x6E, 0x6E, 0x6E, // 32
  0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E,
  //0x1F, 0xAC,
  // 1      2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
};



//-----------------------------------------------------------------------------
// Function PROTOTYPES
//-----------------------------------------------------------------------------

void Delay (void);


                            
 //           LCD_Set_Cursor(0,12);
//            puthexLCD(adcResult>>8);
 //           puthexLCD(adcResult);
//-----------------------------------------------------------------------------
// Global VARIABLES
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// MAIN Routine
//-----------------------------------------------------------------------------


void main_2 (void) {

   UART=1;
                            // select UART: 0 = UART0, 1 = UART1
   rs232_s =0;
	findFC=0;
	idle=0;
	idle2=0;
    rx_bytes=0;
    rx_index=0;
  // DISP_Init();
 //  DISP_ClearAll();
    
    showTitle();

  
   putdata (0xca);
#ifdef  USE_SLEEP
  SLEEP();
#endif  
   while (1) {
       
		if ( rx_bytes!= rx_index)
		{
			idle =0;
            idle2 =0;
            if( lcd_log_flag)
                log_data (~(UART_Buffer[rx_index]));
            
                
		    store_data (UART_Buffer[rx_index]);
		   rx_index++;
		   if (rx_index>=UART_BUFFERSIZE)
				 rx_index=0;
		}
		else
		{
			 
				 idle++;
				 if (idle> MAX_WAIT_TIME2)
				 {


					 idle=0;
					 idle2++;
					 if ( idle2>MAX_WAIT_TIME)
					 {
                         
                          FinishFlag=1;
                       
                     idle=0;
				     idle2=0;  
                   
				    }
			   }
               if(FinishFlag)
               {
                    FinishFlag=0;
                    if( findFC || findB5 )
                	{  
                            if  ( lcd_log_flag)
                            {   
                               if (MultiFlag)
                               {
                                  checksum();
                               }   
                               else
                               {
                                   DataSize=GetDataSize(); 
                                   checksum();


                               }
                              //  display ();
                                DisplayFlag=1;
                                LCD_index=0;
                             }// LCD_log
                        
				            
                            rs232_s =0;
                    	    findFC=0;
                            findB5=0;
                            lcd_log_flag=0;
                            pc_buf_index=0;
                                
                      
					 }//Fin FC
                  idle=0;
				  idle2=0; 
                  #ifdef  USE_SLEEP
                    BAUD1CONbits.WUE=1;
                   if (!DisplayFlag)
                   { 
                     if (lcdOnFlag)
                       ClearLCD();	 
                     SLEEP();
                   }
                  #endif
               }    
          if (DisplayFlag)
          {
              DisplayFlag=0;
              display();
          }
		} // end RS232
        
   }/// end while
}

//-----------------------------------------------------------------------------
// putchar
//-----------------------------------------------------------------------------
//
// Return Value : UART0/1 buffer value
// Parameters   : character to be transmitted across UART0/1
//
// This is an overloaded fuction found in the stdio library.  When the
// function putchar is called, either by user code or through calls to stdio
// routines such as printf, the following routine will be executed instead 
// of the function located in the stdio library.
//
// The function checks the UART global variable to determine which UART to 
// use to receive a character.
//
// The routine expands '\n' to include a carriage return as well as a 
// new line character by first checking to see whether the character  
// passed into the routine equals '\n'.  If it is, the routine waits for 
// SCON0_TI/TI1 to be set, indicating that UART 0/1 is ready to transmit another 
// byte.  The routine then clears SCON0_TI/TI1 bit, and sets the UART0/1 output 
// buffer to '0x0d', which is the ASCII character for carriage return.
//
// The routine the waits for SCON0_TI/TI1 to be set, clears SCON0_TI/TI1, and sets
// the UART output buffer to <c>.  
//
//-----------------------------------------------------------------------------



unsigned char putdata (unsigned char c)
{

     // while (!(SCON1 & 0x02));         // wait until UART1 is ready to transmit
     //  SCON1 &= ~0x02;                  // clear TI1 interrupt flag
      //SBUF1 = c;
        putrs1USART_2(c);  // 呼叫 app.c 的 UART1 函數來送出字串
      return (c);              // output <c> using UART 1


}


//-----------------------------------------------------------------------------
// _getkey
//-----------------------------------------------------------------------------
//
// Return Value : byte received from UART0/1
// Parameters   : none

// This is an overloaded fuction found in the stdio library.  When the
// function _getkey is called, either by user code or through calls to stdio
// routines such as scanf, the following routine will be executed instead 
// of the function located in the stdio library.
//
// The function checks the UART global variable to determine which UART to 
// use to receive a character.
//
// The routine waits for SCON0_RI/RI1 to be set, indicating that a byte has
// been received across the UART0/UART1 RX line.  The routine saves the 
// received character into a local variable, clears the SCON0_RI/RI1 interrupt
// flag, and returns the received character value.
//
//-----------------------------------------------------------------------------

#if defined __C51__

char getchar ()  {
  char c;

  if (UART == 0) {
    while (!SCON0_RI);                      // wait until UART0 receives a character

    getData = SBUF0;                         // save character to local variable
    c=getData;
    SCON0_RI = 0;                           // clear UART0 receive interrupt flag
    return (c);                        // return value received through UART0
  }

  else if (UART == 1) {
    while (!(SCON1 & 0x01));           // wait until UART1 receives a character
    getData = SBUF1;                         // save character to local variable
    c=getData;
    SCON1 &= ~0x01;                    // clear UART1 receive interrupt flag
    return (c);                        // return value received through UART1
  }
  else {
	  return 0xff;
  }
}

#elif defined __ICC8051__
ccc
int getchar(void){
  char c;

  if (UART == 0) {
    while (!SCON0_RI);                      // wait until UART0 receives a character
    c = SBUF0;                         // save character to local variable
    SCON0_RI = 0;                           // clear UART0 receive interrupt flag
    return (c);                        // return value received through UART0
  }

  else if (UART == 1) {
    while (!(SCON1 & 0x01));           // wait until UART1 receives a character
    c = SBUF1;                         // save character to local variable
    SCON1 &= ~0x01;                    // clear UART1 receive interrupt flag
    return (c);                        // return value received through UART1
  }
  else {
	  return 0xff;
  }
}

#endif


//-----------------------------------------------------------------------------
// Delay
//-----------------------------------------------------------------------------
//
// Return Value : none
// Parameters   : none
//
// Used for a small pause of approximately 40 us.
//
//-----------------------------------------------------------------------------

void Delay(void)
{
  unsigned int x;
   for(x = 0;x < 500;x)
      x++;
}
void store_data (unsigned char cc)
{
  
	   switch (rs232_s)
	   {
	      case 0:
	      {
	    	  if (cc==0xFC)
	    	  {
	    		  wait_bytes=0;
	    		  findFC=1;
	    		  idle=0;
	    		  idle2=0;
	    		  rs232_s=1;
                  rs232_FC=0;
	    	  }
	      }
	      break;
	      case 1:
              step01();
	       break;
	      case 2:
              step02(cc);
	      break;
	      case 3:  //// Send header
              step03();
		     break;
	      case 7:
	          step07();
	      break;
	      case 8:
              step08();
	      break;
  //////////////////////////////////////////////////////////////                        
          case 9:
              step09();
          break;
          case 10:
              step10();
	      break;      
  ///////////////////////////////////////////////////////////////////                        
         case 11:
             step11();
          break;                 
         case 12:
              step12();
	     break;               
  ///////////////////////////////////////////////////////////////////                        
         case 13:
               step13();
          break;                 
           case 14:
                step14();
	       break;                                      
           case 18 :
               step18();
           break;
	      default:
            switchdefault (cc);
	       break;
	   }
} // end od store_data
void step01(void)
{
 wait_bytes++;
 if(wait_bytes>=PHASE1)
  {
	putdata (0xca);
	for (j=0;j<500;j++);
	putdata (0x60);
	 wait_bytes=0;
	 rs232_s=2;
   PC_h1_index=0;
   PC_h2_index=0;
   PC_h3_index=0;
   PC_h4_index=0;
  }
}
void step02(unsigned char cc)
{
 wait_bytes++;
 if(PC_h1_index<HEADER_SIZE)
    PC_H1[PC_h1_index++]=cc;
 if (cc==0xEF)
	recFlag=0;
 if (cc==0xF5) // 12 Bytes
 {
    wait_bytes=PHASE2;
	recFlag=1;
    lcd_buf_index=0;
    for ( i=0; i<ZERO_SIZE ; i++)
      LCD_BUF0[lcd_buf_index++]=0x00;
    for ( i=0; i<SIZE1 ; i++)
	  LCD_BUF0[lcd_buf_index++]=0x04;
	 lcd_buf_index=0;
 }
 if (cc==0x63 && (wait_bytes!=11 && wait_bytes!=9) ) // 12 Bytes 11 fix 516 09 fix 568
 {
    wait_bytes=PHASE2;
	recFlag=0;
    lcd_buf_index=0;
 }
 if(wait_bytes>=PHASE2)
 {
   wait_bytes=0;
   if (recFlag)
   {
       if (PC_H1[MULTI_index]== MULTI_BYTE)   
       {
          DataSize=(decode((0xFF-(PC_H1[DATASIZE_h])))<<8 )+decode((0xFF-(PC_H1[DATASIZE_l])));
          DataSize--; // Skip 1 
          MultiFlag=1;  
        } 
        else
        {
          DataSize=0;
          MultiFlag=0;  
        } 
        rs232_s=18;
                           
    } 
	else
	  rs232_s=3;
   }
}
void step03(void)
{
  for (j=0;j<600;j++);
  wait_bytes++;
  if(wait_bytes>=PHASE3 )
  {
	for ( i=0; i<3 ; i++)
	{
	 putdata (Header[i]);
	 for (j=0;j<100;j++);
	 }
	  rs232_s=4;
	   wait_bytes=0;
   }
  ////////////////////////
   wait_bytes++;
  if(wait_bytes>=PHASE4 )
  {
	for ( i=0; i<ZERO_SIZE ; i++)
	{
      if(MultiFlag)
         putdata (0x00);  
       else
       putdata (LCD_BUF0[lcd_buf_index++]);
       for (j=0;j<100;j++);
	} 
	wait_bytes=0;
	rs232_s=6; //end
  }
 //////////////////////////////////
  for (j=0;j<600;j++);
  wait_bytes++;
  if(wait_bytes > (unsigned char)PHASE6 )
  {
    for ( i=0; i<FOUR_SIZE ; i++)
	{
	  if(MultiFlag)   
       putdata (LCD_BUF1[lcd_buf_index++]);
      else
       putdata (LCD_BUF0[lcd_buf_index++]);
	   for (j=0;j<100;j++);
	}
	wait_bytes=0;
	rs232_s=7; //end
  }
}
void step07 (void)
{
 for (j=0;j<600;j++);
 for (j=0;j<600;j++);
 wait_bytes++;
 if(wait_bytes>=PHASE7 )
 {
    for ( i=0; i<10 ; i++)
	{
	  if(MultiFlag)   
         putdata (LCD_BUF1[lcd_buf_index++]);
      else
          putdata (LCD_BUF0[lcd_buf_index++]);
	   for (j=0;j<100;j++);
	}
	wait_bytes=0;
	rs232_s=8; //end
  }
}
void step08(void)
{
  wait_bytes++;
  if(wait_bytes>=PHASE7 )
  {
    for ( i=0; i<7 ; i++)
	{
	  if(MultiFlag)   
        putdata (LCD_BUF1[lcd_buf_index++]);
       else
        putdata (LCD_BUF0[lcd_buf_index++]);
	    for (j=0;j<100;j++);
	}
	wait_bytes=0;
    lcd_buf_index=0;
    if(MultiFlag)
	  rs232_s=9; //end
     else
     {
    //   FinishFlag=1;  
       rs232_s=100; //end
     }
  }
}  
void step09(void)
{
 wait_bytes++;
 if(wait_bytes>=PHASE3 )
 {
     sendHeadOut (DataSize,CHECKSUM_BUF[DATASIZE],1);
     wait_bytes=0;
	 rs232_s=10; //end
 }  
}
void step10(void)
{
 wait_bytes++;
 if(wait_bytes>=PHASE7 )
 {
    for (j=0;j<6553;j++);
	for (j=0;j<6553;j++);
    for (j=0;j<6553;j++);
    for (j=0;j<600;j++);
    for (j=0;j<600;j++);
	for (j=0;j<600;j++);
	for (j=0;j<600;j++);
	for (j=0;j<600;j++);
	for (j=0;j<600;j++);
	for (j=0;j<600;j++);
	for (j=0;j<600;j++);
    if (DataSize<=DATA_MAX_SIZE)
    { 
      unsigned int end=DataSize;
      for ( i=0; i<end ; i++)
      {
       //  if (decode(LCD_BUF[i]) < ENTER_CODE ) break; 
         putdata (LCD_BUF[i]);
          for (j=0;j<100;j++);
      }
    //  i++;
      for (j=0;j<6000;j++);
       putdata (CHECKSUM_BUF[CHECKSUM3_L]);
       for (j=0;j<100;j++);
       for (j=0;j<6000;j++);
       putdata (CHECKSUM_BUF[CHECKSUM3_H]);
       for (j=0;j<100;j++);
       wait_bytes=0;
        FinishFlag=1;  
        for (j=0;j<60000;j++);
       rs232_s=100; //end
    }    
    else   
     if (DataSize<=DATA_MAX_SIZE2)
     {
         sendDataOut (0 ,DATA_MAX_SIZE , CHECKSUM_BUF[CHECKSUM1_L] ,CHECKSUM_BUF[CHECKSUM1_H]) ; 
         wait_bytes=0;
         rs232_s=11; //end
     }
     else
     {
        sendDataOut (0 ,DATA_MAX_SIZE , CHECKSUM_BUF[CHECKSUM1_L] ,CHECKSUM_BUF[CHECKSUM1_H]) ;
        wait_bytes=0;
        rs232_s=11; //end
     }
}// end wait
}
void step11(void)
{
  wait_bytes++;
  if(wait_bytes>=PHASE3 )
  {
    sendHeadOut ((DataSize-DATA_MAX_SIZE),CHECKSUM_BUF[DATASIZE],0);
    wait_bytes=0;
	rs232_s=12; //end
  }  
}
void step12(void)
{
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  wait_bytes++;
  if(wait_bytes>=PHASE7 )
  {
    if (DataSize<=DATA_MAX_SIZE2)
    { 
       unsigned int end=(DataSize-DATA_MAX_SIZE);
       sendDataOut (0 , end , CHECKSUM_BUF[CHECKSUM3_L] ,CHECKSUM_BUF[CHECKSUM3_H]);                                 wait_bytes=0;
       rs232_s=100; //end
    }  
    else
    {   
        sendDataOut (0 , (DATA_MAX_SIZE) , CHECKSUM_BUF[CHECKSUM2_L] ,CHECKSUM_BUF[CHECKSUM2_H]);    
         wait_bytes=0;
         rs232_s=13; //end
     }    
  }  
}
void step13(void)
{
  wait_bytes++;
  if(wait_bytes>=PHASE3 )
  {
#ifdef FOR_TEST
       for (j=0;j<6553;j++);
     for (j=0;j<500;j++);
  	 for ( i=0; i<3 ; i++)
	  {
	    putdata (Header2[i]);
	    for (j=0;j<100;j++);
	  }
#else      
        sendHeadOut ((DataSize-DATA_MAX_SIZE-DATA_MAX_SIZE),CHECKSUM_BUF[DATASIZE],0);
#endif        
        /*
     for (j=0;j<6553;j++);
     for (j=0;j<500;j++);
      putdata (Header2[i]
	 for ( i=0; i<3 ; i++)
	  {
	    putdata (Header2[i]);
	    for (j=0;j<100;j++);
	  }
         * */
      wait_bytes=0;
	   rs232_s=14; //end
   }  
}
void step14(void)
{
  for (j=0;j<6553;j++);
  for (j=0;j<6553;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  for (j=0;j<600;j++);
  wait_bytes++;
  if(wait_bytes>=PHASE7 )
  {
     unsigned int end=(DataSize-DATA_MAX_SIZE-DATA_MAX_SIZE);
    
     sendDataOut (0 , end , CHECKSUM_BUF[CHECKSUM3_L] ,CHECKSUM_BUF[CHECKSUM3_H]);
     wait_bytes=0;
    // FinishFlag=1;  
	 rs232_s=100; //end
  }
}
void step18(void)
{
    wait_bytes++;
    if(wait_bytes > (unsigned char)PHASE18 )
    {
       lcd_log_flag=1;
       wait_bytes=0;
       if (MultiFlag)
       {
           
       }
       else
       {
          
           idle=(MAX_WAIT_TIME-3);
           idle2=(MAX_WAIT_TIME2-3);
       }
       rs232_s=100; //end
    }
           

}
void switchdefault (unsigned char cc)
{
  wait_bytes++;
  if (recFlag)
   {   
      if(MultiFlag)
      {    
                switch (rs232_FC)
                 {
                     case 0:
                     {    
                        if (cc==0xFC)
                         rs232_FC=1;
                     }    
                      break; 
                      case 1:
                     {    
                          if (cc==0xFF)
                              rs232_FC=2;
                          else
                              rs232_FC=0;
                      }    
                      break; 
                      case 2:
                      {    
                        if (cc==0xB5)
                            rs232_FC=4;
                         else
                            rs232_FC=0;
                      }    
                      break; 
                       case 3:
                       {    
                          if (cc==0xFF)
                              rs232_FC=4;
                            else
                               rs232_FC=0;
                        }    
                        break; 
                        case 4:
                        {    
                            if (cc==0xFF)
                           {
                              wait_bytes=0;
                              findB5=1;
                              idle=0;
                               idle2=0;
                               putdata (0xca);
                               for (j=0;j<500;j++);
                                 putdata (0x60);
                                 for (j=0;j<500;j++);
                                 wait_bytes=0;
                                 wait_bytes=0;
                                 if (MultiFlag)
                                 {
                                     MultiFlag++;
                                     if (MultiFlag>=MAX_MULTIFLAG)
                                     {
                                            idle=(MAX_WAIT_TIME+3);
                                            idle2=(MAX_WAIT_TIME2+3);
                                            FinishFlag=1;
                                     }
                                 }    
                                 rs232_FC=1;
                            }  
                            else
                               rs232_FC=0;
                            }    
                            break; 
                 } // end switch  
        }else { //// Not MultiFlag
          if(wait_bytes> MAX_ONEFLAG)
              FinishFlag=1;
        }
     } // end if not recFlag
     else
     {
         idle=(MAX_WAIT_TIME)/SPEEDUP;
         idle2=(MAX_WAIT_TIME2)/SPEEDUP;
     }  
}
void sendHeadOut (unsigned int DataSS,unsigned int DataS ,unsigned char flag)
{
   for (j=0;j<6553;j++);
   if (flag)
   {    
      putdata (0xca);
      for (j=0;j<500;j++);
	  putdata (0x60);
     for (j=0;j<500;j++);
   }
    if (DataSS<=DATA_MAX_SIZE)
      Header2[1]=DataS;
    else
      Header2[1]=0x81;
    
	 for ( i=0; i<3 ; i++)
	 {
       if (DataSS==(DATA_MAX_SIZE-1))
          putdata (Header3[i]);
       else        
       if(DataSS<DATA_MAX_SIZE)
          putdata (Header2[i]);
        else 
	       putdata (Header[i]);
	    for (j=0;j<100;j++);
	  }
}
void sendDataOut (unsigned int start , unsigned int ended , unsigned char checksuml ,unsigned char checksumh )
{
         for ( i=start; i<ended ; i++)
         {
            putdata (LCD_BUF[lcd_buf_index++]);
            for (j=0;j<100;j++);
         }
        putdata ( checksuml);
        for (j=0;j<100;j++);
        putdata ( checksumh );
}

void log_data(unsigned char cc)
{
#ifdef PC_BUF_DEBUG  
    if (pc_buf_index<MAX_PC_BUFFER)
      PC_BUF[pc_buf_index]  =cc;
#endif      
      if (MultiFlag)
      {   
          if(DataSize > SKIP1)
          {    
            if(  (pc_buf_index > SKIP1) && (pc_buf_index <= (unsigned int)(SKIP1+SKIP1_SIZE)))
               {
                 PC_H2[PC_h2_index++]  =decode(cc);
               }
            else
            if(  (pc_buf_index > SKIP2) && (pc_buf_index <= (unsigned int)(SKIP2+SKIP2_SIZE)))
               {
                 PC_H3 [PC_h3_index++]  =decode(cc);
               }
            else
            {   
                LCD_BUF[lcd_buf_index]  =cc;
                lcd_buf_index++;
            }
          } 
          else
          {
                
                LCD_BUF[lcd_buf_index]  =cc;
                lcd_buf_index++;
          }   

      }     
      else  ///////////////// MultiFlag
      {
         LCD_BUF[lcd_buf_index]  =decode(cc);
         lcd_buf_index++;
      }    
     
    
      pc_buf_index++;

}
unsigned char decode ( unsigned char cc)
{
    unsigned char aa =0;
    if (cc & 0x80)  aa |=0x01;
    if (cc & 0x40)  aa |=0x02;
    if (cc & 0x20)  aa |=0x04;
    if (cc & 0x10)  aa |=0x08;
    
    if (cc & 0x08)  aa |=0x10;
    if (cc & 0x04)  aa |=0x20;
    if (cc & 0x02)  aa |=0x40;
    if (cc & 0x01)  aa |=0x80;
    return aa;
}
unsigned char encode ( unsigned char cc)
{
    unsigned char aa =0;
    if (cc & 0x80)  aa |=0x01;
    if (cc & 0x40)  aa |=0x02;
    if (cc & 0x20)  aa |=0x04;
    if (cc & 0x10)  aa |=0x08;
    
    if (cc & 0x08)  aa |=0x10;
    if (cc & 0x04)  aa |=0x20;
    if (cc & 0x02)  aa |=0x40;
    if (cc & 0x01)  aa |=0x80;
    return aa;
}

void checksum (void)
{
   unsigned int checksum;
   unsigned char checksumhi;
   unsigned int checksum2;
   realDataSize=DataSize;  
   unsigned int tailDataSize=(decode(LCD_BUF[(lcd_buf_index-SKIP2_SIZE)] )<<8) +decode(LCD_BUF[(lcd_buf_index-SKIP2_SIZE+1)]);
   
   if(MultiFlag) 
   {
        
       if (DataSize<= DATA_MAX_SIZE)
       {
       
        
        if (DataSize >=PC1_SIZE)    /////// 250~256
        {   
            checksum = (PC_H2[2] <<8) +PC_H2[0] ;
            for (i=PC1_SIZE;i<(DataSize);i++)
            {  
                if (decode(LCD_BUF[i]) < ENTER_CODE ) break;
               checksum +=decode(LCD_BUF[ i]);
            }
            
            checksumhi=(char)(checksum >> 8);
            CHECKSUM_BUF[CHECKSUM3_H]=encode((char)checksumhi);
            CHECKSUM_BUF[CHECKSUM3_L]=encode((char)(checksum & 0x00FF));
         
            
             checksum= tailDataSize;
        }
        else
        {
           CHECKSUM_BUF[CHECKSUM3_L]=LCD_BUF[DataSize];
           CHECKSUM_BUF[CHECKSUM3_H]=LCD_BUF[DataSize+2];
           
         
           checksum= tailDataSize;
        }
         checksumhi=(char)(checksum >> 8);
         CHECKSUM_BUF[CHECKSUM1_H]=encode((char)checksumhi);
         CHECKSUM_BUF[CHECKSUM1_L]=encode((char)(checksum & 0x00FF));
        
        checksumhi=(char)(checksum >> 8);
        LCD_BUF1[TAIL_ADDR1]=encode((char)checksumhi);
        LCD_BUF1[TAIL_ADDR2]=encode((char)(checksum & 0x00FF));
        
        
        checksum2=TAIL_BASE+(checksum & 0x00FF)+ checksumhi    ;
        checksumhi=(char)(checksum2 >> 8);
        LCD_BUF1[TAIL_ADDR3]=encode((char)(checksum2 & 0x00FF));
        LCD_BUF1[TAIL_ADDR4]=encode((char)checksumhi);
        CHECKSUM_BUF[DATASIZE]=encode(DataSize+1);
       }
       else
        if (DataSize <= DATA_MAX_SIZE2)   
       {
            // Jason 
          if (DataSize >=461&& DataSize<=479)
          {   
            checksum= TAIL_CHECKSUM_BASE+ (DataSize-1);
             LCD_BUF1[TAIL_ADDR0]=0x44;
             LCD_BUF1[TAIL_ADDR00]=0x03;
          }
           else
           checksum= tailDataSize;   
        checksumhi=(char)(checksum >> 8);
        LCD_BUF1[TAIL_ADDR1]=encode((char)checksumhi);
        LCD_BUF1[TAIL_ADDR2]=encode((char)(checksum & 0x00FF));
        
        if (DataSize >=461&& DataSize<=479)
          checksum2=TAIL_BASE+(checksum & 0x00FF)+ checksumhi  +0x22+0xC0  ;
        else
          checksum2=TAIL_BASE+(checksum & 0x00FF)+ checksumhi ;
        checksumhi=(char)(checksum2 >> 8);
        LCD_BUF1[TAIL_ADDR3]=encode((char)(checksum2 & 0x00FF));
        LCD_BUF1[TAIL_ADDR4]=encode((char)checksumhi);
        
           checksum = (PC_H2[2] <<8) +PC_H2[0] ;
        
           checksum +=( decode(LCD_BUF[ ADJ1_ADDR1])+decode( LCD_BUF[ ADJ1_ADDR2]) + decode(LCD_BUF[ ADJ1_ADDR3])
                   + decode(LCD_BUF[ ADJ1_ADDR4])+ decode(LCD_BUF[ ADJ1_ADDR5])+ decode(LCD_BUF[ ADJ1_ADDR6]));
        
           checksumhi=(char)(checksum >> 8);
           CHECKSUM_BUF[CHECKSUM1_H]=encode((char)checksumhi);
           CHECKSUM_BUF[CHECKSUM1_L]=encode((char)(checksum & 0x00FF));
           
           checksum=CHECKSUM_BASE;
           
           for (i=DATA_MAX_SIZE;i<(DataSize);i++)
                  checksum+=decode(LCD_BUF[ i]);
             
           
           checksumhi=(char)(checksum >> 8);
           CHECKSUM_BUF[CHECKSUM3_H]=encode((char)checksumhi);
           CHECKSUM_BUF[CHECKSUM3_L]=encode((char)(checksum & 0x00FF));
           
           CHECKSUM_BUF[DATASIZE]=encode(((DataSize-DATA_MAX_SIZE+1)));
       }
        else 
       {
           checksum = (PC_H2[2] <<8) +PC_H2[0] ;
           checksum =checksum + decode(LCD_BUF[ ADJ1_ADDR1])+decode( LCD_BUF[ ADJ1_ADDR2]) + decode(LCD_BUF[ ADJ1_ADDR3])
                   + decode(LCD_BUF[ ADJ1_ADDR4])+ decode(LCD_BUF[ ADJ1_ADDR5])+ decode(LCD_BUF[ ADJ1_ADDR6]);
           checksumhi=(char)(checksum >> 8);
           CHECKSUM_BUF[CHECKSUM1_H]=encode((char)checksumhi);
           CHECKSUM_BUF[CHECKSUM1_L]=encode((char)(checksum & 0x00FF));
           
#ifdef GET_CHECKSUM_PC
            checksum = (PC_H3[2] <<8) +PC_H3[0] ;
           
          checksum =checksum + decode(LCD_BUF[ ADJ2_ADDR1])+decode( LCD_BUF[ ADJ2_ADDR2]) + decode(LCD_BUF[ ADJ2_ADDR3])
                  + decode(LCD_BUF[ ADJ2_ADDR4])+ decode(LCD_BUF[ ADJ2_ADDR5])+ decode(LCD_BUF[ ADJ2_ADDR6]);
           checksum =checksum - decode(LCD_BUF[ ADJ1_ADDR1])-decode( LCD_BUF[ ADJ1_ADDR2]) - decode(LCD_BUF[ ADJ1_ADDR3])
                   - decode(LCD_BUF[ ADJ1_ADDR4])- decode(LCD_BUF[ ADJ1_ADDR5])-decode(LCD_BUF[ ADJ1_ADDR6]);
           
           checksum =checksum + decode(LCD_BUF[ ADJ2_ADDR7])+decode( LCD_BUF[ ADJ2_ADDR8]) + decode(LCD_BUF[ ADJ2_ADDR9])
                   + decode(LCD_BUF[ ADJ2_ADDR10])+ decode(LCD_BUF[ ADJ2_ADDR11])+ decode(LCD_BUF[ ADJ2_ADDR12]);
           
            checksumhi=(char)(checksum >> 8);
           CHECKSUM_BUF[CHECKSUM2_H]=encode((char)checksumhi);
           CHECKSUM_BUF[CHECKSUM2_L]=encode((char)(checksum & 0x00FF));
#else           
           checksum =CHECKSUM_BASE;
            for (i=DATA_MAX_SIZE;i<DATA_MAX_SIZE2;i++)
               checksum+=decode(LCD_BUF[ i]);
           
           checksumhi=(char)(checksum >> 8);
           CHECKSUM_BUF[CHECKSUM2_H]=encode((char)checksumhi);
           CHECKSUM_BUF[CHECKSUM2_L]=encode((char)(checksum & 0x00FF));
          ////////////////////////////////////////////////////////////////////// 
            checksum = (PC_H3[2] <<8) +PC_H3[0] ;
           
          checksum =checksum + decode(LCD_BUF[ ADJ2_ADDR1])+decode( LCD_BUF[ ADJ2_ADDR2]) + decode(LCD_BUF[ ADJ2_ADDR3])
                  + decode(LCD_BUF[ ADJ2_ADDR4])+ decode(LCD_BUF[ ADJ2_ADDR5])+ decode(LCD_BUF[ ADJ2_ADDR6]);
           checksum =checksum - decode(LCD_BUF[ ADJ1_ADDR1])-decode( LCD_BUF[ ADJ1_ADDR2]) - decode(LCD_BUF[ ADJ1_ADDR3])
                   - decode(LCD_BUF[ ADJ1_ADDR4])- decode(LCD_BUF[ ADJ1_ADDR5])-decode(LCD_BUF[ ADJ1_ADDR6]);
           
           checksum =checksum + decode(LCD_BUF[ ADJ2_ADDR7])+decode( LCD_BUF[ ADJ2_ADDR8]) + decode(LCD_BUF[ ADJ2_ADDR9])
                   + decode(LCD_BUF[ ADJ2_ADDR10])+ decode(LCD_BUF[ ADJ2_ADDR11])+ decode(LCD_BUF[ ADJ2_ADDR12]);
           
            checksumhi=(char)(checksum >> 8);
           CHECKSUM_BUF[CHECKSUM4_H]=encode((char)checksumhi);
           CHECKSUM_BUF[CHECKSUM4_L]=encode((char)(checksum & 0x00FF));
#endif           
           ///////////////////////////////////////////////////////
           checksum=CHECKSUM_BASE;
           
           for (i=DATA_MAX_SIZE2;i<(DataSize);i++)
           {
              checksum+=decode(LCD_BUF[ i]);
           }
        //   checksum = (decode(LCD_BUF[DataSize+1]) <<8) +decode(LCD_BUF[DataSize-1]) ;
         //  checksum -=( decode(LCD_BUF[ ADJ2_ADDR1])+decode( LCD_BUF[ ADJ2_ADDR2]) + decode(LCD_BUF[ ADJ2_ADDR3])
          //         + decode(LCD_BUF[ ADJ2_ADDR4])+ decode(LCD_BUF[ ADJ2_ADDR5])+ decode(LCD_BUF[ ADJ2_ADDR6]));
           //checksum -=( MID_BUF[ ADJ2_ADDR1]+ MID_BUF[ ADJ2_ADDR2] + MID_BUF[ ADJ2_ADDR3]
           //      + MID_BUF[ ADJ2_ADDR4]+ MID_BUF[ ADJ2_ADDR5]+ MID_BUF[ ADJ2_ADDR6]);
           checksumhi=(char)(checksum >> 8);
           CHECKSUM_BUF[CHECKSUM3_H]=encode((char)checksumhi);
           CHECKSUM_BUF[CHECKSUM3_L]=encode((char)(checksum & 0x00FF));
           
           
              checksum= tailDataSize;   
      //  checksum= TAIL_CHECKSUM_BASE+(DataSize-6); //// Why ????
        //   checksum=(decode(PC_BUF[DataSize+1])<<8+decode(PC_BUF[DataSize-1]))-0x28A;
        checksumhi=(char)(checksum >> 8);
        LCD_BUF1[TAIL_ADDR1]=encode((char)checksumhi);
        LCD_BUF1[TAIL_ADDR2]= encode((char)(checksum & 0x00FF));
        checksum2=TAIL_BASE+(checksum & 0x00FF)+ checksumhi    ;
        checksumhi=(char)(checksum2 >> 8);
        LCD_BUF1[TAIL_ADDR3]=encode((char)(checksum2 & 0x00FF));
        LCD_BUF1[TAIL_ADDR4]=encode((char)checksumhi);
        
        CHECKSUM_BUF[DATASIZE]=encode(((DataSize-DATA_MAX_SIZE+1)));
       } 
   }
   else
   {    
                checksum=BASE1+ (BASEH << 8);
                unsigned char range1=DataSize%LCD_SIZE;
                unsigned char range2=(DataSize/LCD_SIZE)*LCD_SIZE;
                unsigned char range3=RANG1-range2;
                if (!range1)
                {
                    range2=(DataSize/LCD_SIZE-1)*LCD_SIZE;
                    range3=RANG1-range2;
                }   
                if (range2)
                {    
                  for (i=range3;i<RANG1;i++)
                      checksum +=( LCD_BUF[i]);
                  checksum-=range2;
                
                }
                for (i=0;i<range1;i++)
                {
                    checksum +=( LCD_BUF[RANG1+i]);
                    checksum -=EMPTY1;

                }
                
                if (!range1)
                {   
                     for (i=0;i<LCD_SIZE;i++)
                    {
                         checksum +=( LCD_BUF[RANG1+i]);
                          checksum -=EMPTY1;

                    }
                }    
                 checksumhi=(char)(checksum >> 8);

                LCD_BUF0[CHECKSUMH] = encode ( checksumhi);
                LCD_BUF0[CHECKSUM1] = encode ((char)(checksum & 0x00FF) );
                
   } 
}

void display (void)
{
    ClearLCD();
 if (MultiFlag) 
 {
        if (DataSize < ( LCD_index+LCD_SIZE))
        {
            LCD_Set_Cursor(0,0); 
          //  for (i=LCD_index;i<(LCD_index+DataSize );i++)
            for (i=LCD_index;i<(DataSize );i++)
             putcLCD(decode(LCD_BUF[i]));
            LCD_index=0;
        }  
        else
        if (DataSize < ( LCD_index+LCD_SIZE+LCD_SIZE))
        {
              LCD_Set_Cursor(0,0); 
            for (i=LCD_index;i<(LCD_index+ LCD_SIZE );i++)
             putcLCD(decode(LCD_BUF[i]));
               LCD_Set_Cursor(1,0); 
            LCD_index+= LCD_SIZE;  
          ///  for (i=LCD_index;i<(LCD_index+ DataSize );i++)
            for (i=LCD_index;i<(DataSize );i++)
             putcLCD(decode(LCD_BUF[i]));  
            LCD_index=0;
        }   
        else
        {
               LCD_Set_Cursor(0,0); 
            for (i=LCD_index;i<(LCD_index+ LCD_SIZE );i++)
             putcLCD(decode(LCD_BUF[i]));
               LCD_Set_Cursor(1,0); 
            LCD_index+= LCD_SIZE;  
            for (i=LCD_index;i<(LCD_index+ LCD_SIZE );i++)
             putcLCD(decode(LCD_BUF[i]));  
            LCD_index+= LCD_SIZE; 
        }
 }
 else ///////////// Not MultiFlag
 {
    unsigned char range1=DataSize%LCD_SIZE;
    unsigned char range2=(DataSize/LCD_SIZE)*LCD_SIZE;
    unsigned char range3=RANG1-range2;
    
    if (DataSize < ( LCD_index+LCD_SIZE))
    {
         LCD_Set_Cursor(0,0);
         for (i= (RANG1);i<(RANG1+range1);i++)
         putcLCD(decode(LCD_BUF0[i]));
          LCD_index=0;
    }
    else
     if (DataSize < ( LCD_index+LCD_SIZE+LCD_SIZE))
     {
           LCD_Set_Cursor(0,0);
           for (i=(LCD_index+range3);i<(LCD_index+LCD_SIZE+range3);i++)
                putcLCD(decode(LCD_BUF0[i]));
             LCD_index+= LCD_SIZE;  
           LCD_Set_Cursor(1,0); 
          for (i=RANG1;i<(RANG1+range1);i++) 
             putcLCD(decode(LCD_BUF0[i]));   
           LCD_index=0;
     }
     else
     {
        LCD_Set_Cursor(0,0); 
        for (i=(range3+LCD_index);i<(LCD_index+ LCD_SIZE+range3 );i++)
         putcLCD(decode(LCD_BUF0[i]));
           LCD_Set_Cursor(1,0); 
        LCD_index+= LCD_SIZE;  
        for (i=(LCD_index+range3);i<(LCD_index+ LCD_SIZE+range3 );i++)
         putcLCD(decode(LCD_BUF0[i]));  
        LCD_index+= LCD_SIZE; 
     }
 
 }    
 
    lcdOnFlag=1;
}   

unsigned int GetDataSize (void)
{
    unsigned char D0_index=D0_LOC;
    unsigned int dd;
    D0_index--;
    while (LCD_BUF[D0_index] == EMPTY_DATA) D0_index--;
    times16=(D0_BASE-LCD_BUF[D0_LOC]);
    for (i=0;i<=times16;i++)
        LCD_BUF0[RANG1-i-1]=encode(LCD_BUF[RANG1-i-1]);
    for (i=RANG1;i<=D0_index;i++)
           LCD_BUF0[i]=decode(LCD_BUF[i]);
    dd=(D0_index-RANG1+1)+ times16;
    #ifdef PC_BUF_DEBUG
        LCD_BUF0[D0_LOC]=PC_BUF[D0_LOC];
    #else
       LCD_BUF0[D0_LOC]=decode(LCD_BUF[D0_LOC]);
    #endif
    return dd;
}

void button(void)
{
    DisplayFlag=1;
 //   display();
}


void showTitle(void)
{
     ClearLCD();
    LCD_Set_Cursor(0,0);                          
    putcLCD('I');
    putcLCD('-');
    putcLCD('M');
    putcLCD('o');
    putcLCD('b');
    putcLCD('i');
    putcLCD('l');
    putcLCD('e');
    putcLCD(' ');
    putcLCD('V');
    putcLCD('1');
    putcLCD('.');
    putcLCD('0');
    putcLCD('4');
  
}
//-----------------------------------------------------------------------------
// End Of File
//-----------------------------------------------------------------------------
