/*****************************************************************************
 *
 * Filename:
 * ---------
 *    Motion_sensor_custom.c
 *
 * Project:
 * --------
 *    Motion Sensor KX023 I2C Interface
 *
 * Description:
 * ------------
 *   This Module defines Serial Interface.
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 ****************************************************************************/
#include "mcc_generated_files/mcc.h"
#include <stdio.h>                  // ??? itoa() ??
#include "motion_sensor_custom.h"
#include "def.h"
#ifdef MOTION_SENSOR_SUPPORT

#include "motion_sensor_custom.h"   
#include "motion_sensor_iic.h"   

/* ---------------------------------------------------------------------------- */
/*
* FUNCTION                                                            
*	motion_sensor_check_softwave_ready
*
* DESCRIPTION                                                           
*   check sensor softwave function completion
*
* PARAMETERS
*  received data
*	
* RETURNS
*	None
*/    
void motion_sensor_check_softwave_ready(void)
{    
	do
	{
		MS_Delay(50);
	} while ((motion_sensor_read_data(MOTION_SENSOR_CTRL_REGB, MOTION_SENSOR_ADDRESS) & 0x80));
	
	return ;
}


/*
* FUNCTION                                                            
*	acc_sensor_pwr_up
*
* DESCRIPTION                                                           
*   Power up acceleration module
*
* CALLS  
*
* PARAMETERS
*  None
*	
* RETURNS
*	None
*/    
void acc_sensor_pwr_up(void)
{
	MS_Delay(30);//delay for power on reset	
	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGB, MOTION_SENSOR_ADDRESS);	//KXTJ2 reset
	motion_sensor_check_softwave_ready();												//wait & check reset completion
	
	motion_sensor_write_data(0x00, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);		//into standby mode
//	motion_sensor_write_data(0x40, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);		//set to 16 bit, 2g
    motion_sensor_write_data(0x40, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);		//set to 16 bit, 2g
	motion_sensor_write_data(0x02, MOTION_SENSOR_ODCNTL_REGC, MOTION_SENSOR_ADDRESS);	//set ODR 50Hz
	motion_sensor_write_data(0x10,MOTION_SENSOR_INT_CTRL_REG1, MOTION_SENSOR_ADDRESS);	//disable all INT1 interrupt funs
	motion_sensor_write_data(0x10,MOTION_SENSOR_INT_CTRL_REG5, MOTION_SENSOR_ADDRESS);	//disable all INT2 interrupt funs
	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);	//set to work mode
}


/*
* FUNCTION                                                            
*	acc_sensor_pwr_down 
*
* DESCRIPTION                                                           
*   power down acceleration
*
* CALLS  
*
* PARAMETERS
*	None
*	
* RETURNS
*	None
*/ 
void acc_sensor_pwr_down(void)
{   
	motion_sensor_disable_bits(0x80, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);	// into standby mode
}


/*
* FUNCTION                                                            
*	acc_sensor_init
*
* DESCRIPTION                                                           
*   Initialization acceleration module
*
* CALLS  
*
* PARAMETERS
*	None
*	
* RETURNS
*	None
*/ 
void acc_sensor_init(void)
{  
	motion_senosr_interface_init();
}


/*
* FUNCTION                                                            
*	acc_sensor_interrupt_initial
*
* DESCRIPTION                                                           
*   Set & enable interrupt function
*
* CALLS  
*
* PARAMETERS
*  None
*	
* RETURNS
*	None
*/    
void acc_sensor_interrupt_initial(void)
{
	motion_sensor_disable_bits(0x80, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);		// into standby mode
	motion_sensor_write_data(0x00, MOTION_SENSOR_INT_CTRL_REG1, MOTION_SENSOR_ADDRESS);
	//motion_sensor_enable_bits(0x30, MOTION_SENSOR_INT_CTRL_REG1, MOTION_SENSOR_ADDRESS);	// interrupt-pin (5) enable, active high, latch, no WUF
  
     motion_sensor_write_data(0x02, MOTION_SENSOR_INT_CTRL_REG4, MOTION_SENSOR_ADDRESS);	// interrupt-pin (5) enable, active high, latch, no WUF
	
     motion_sensor_write_data(0x3F, MOTION_SENSOR_INT_CTRL_REG3, MOTION_SENSOR_ADDRESS);	// interrupt-pin (5) enable, active high, latch, no WUF
	
    
    motion_sensor_write_data(0x3F, MOTION_SENSOR_INT_CTRL_REG2, MOTION_SENSOR_ADDRESS);	// interrupt-pin (5) enable, active high, latch, no WUF
	
    motion_sensor_enable_bits(0x20, MOTION_SENSOR_INT_CTRL_REG1, MOTION_SENSOR_ADDRESS);	// interrupt-pin (5) enable, active high, latch, no WUF
	
	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);		// set to work mode 
}


/*
* FUNCTION                                                            
*	acc_sensor_WUF_initial
*
* DESCRIPTION                                                           
*   Set & enable the WUF function
*
* CALLS  
*
* PARAMETERS
*  None
*	
* RETURNS
*	None
*/    
void acc_sensor_wuf_initial(void)
{
	motion_sensor_disable_bits(0x80, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);	// into standby mode
	motion_sensor_write_data(0x00, MOTION_SENSOR_WUFC, MOTION_SENSOR_ADDRESS);			// WUF_TIMER
	//motion_sensor_write_data(0x08, MOTION_SENSOR_ATH, MOTION_SENSOR_ADDRESS);			//  WUF_THRESH
    motion_sensor_write_data(0x01, MOTION_SENSOR_ATH, MOTION_SENSOR_ADDRESS);			
    motion_sensor_enable_bits(0x02, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS); 	// enable WUF function  
	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGA, MOTION_SENSOR_ADDRESS);	// set to work mode 
}


/*
* FUNCTION                                                            
*	acc_sensor_clear_interrupt_status
*
* DESCRIPTION                                                           
*   Release the latch mode of interrupt
*
* CALLS  
*
* PARAMETERS
*  None
*	
* RETURNS
*	None
*/
void acc_sensor_clear_interrupt_status(void)
{
	char i;
	i = motion_sensor_read_data(MOTION_SENSOR_INT_REL, MOTION_SENSOR_ADDRESS);
}


/*
* FUNCTION                                                            
*	acc_sensor_read_status
*
* DESCRIPTION                                                           
*    Read the interrupt status
*
* CALLS  
*
* PARAMETERS
*  *status1_value : status 1 value 
*  *status2_value : status 2 value 
*	 
* RETURNS
*	None
*/
void acc_sensor_read_status(uint8_t *status1_value, uint8_t *status2_value)
{
	*status1_value = motion_sensor_read_data(MOTION_SENSOR_INS2, MOTION_SENSOR_ADDRESS);
	*status2_value = motion_sensor_read_data(MOTION_SENSOR_INT_STATUS, MOTION_SENSOR_ADDRESS);
}


/*
* FUNCTION                                                            
*	acc_sensor_get_acc
*
* DESCRIPTION                                                           
*   get value in every axis
*
* CALLS  
*
* PARAMETERS
*	x_value : X-axis value
*	y_value : Y-axis value
*	z_value : Z-axis value
*
* RETURNS
*	None
*/ 
void acc_sensor_get_acc(uint16_t *x_value, uint16_t *y_value, uint16_t *z_value)
{	
	uint8_t temp_data[5];
    
    motion_sensor_read_multi_data(temp_data, MOTION_SENSOR_XOUT_L, MOTION_SENSOR_ADDRESS, 1);
    motion_sensor_read_multi_data(&temp_data[1], MOTION_SENSOR_XOUT_H, MOTION_SENSOR_ADDRESS, 1);
    
    
	*x_value = ((temp_data[1] << 8) | temp_data[0]);		// x-axis       
	 motion_sensor_read_multi_data(temp_data, MOTION_SENSOR_YOUT_L, MOTION_SENSOR_ADDRESS, 1);
	 motion_sensor_read_multi_data(&temp_data[1], MOTION_SENSOR_YOUT_H, MOTION_SENSOR_ADDRESS, 1);
	*y_value = ((temp_data[1] << 8) | temp_data[0]);		// x-axis       
     motion_sensor_read_multi_data(temp_data, MOTION_SENSOR_ZOUT_L, MOTION_SENSOR_ADDRESS,1);
	 motion_sensor_read_multi_data(&temp_data[1], MOTION_SENSOR_ZOUT_H, MOTION_SENSOR_ADDRESS, 1);
	*z_value = ((temp_data[1] << 8) | temp_data[0]);		// x-axis       
/*
	motion_sensor_read_multi_data(temp_data, MOTION_SENSOR_XOUT_L, MOTION_SENSOR_ADDRESS, 6);
	
	*x_value = ((temp_data[1] << 8) | temp_data[0]);		// x-axis       
	*y_value = ((temp_data[3] << 8) | temp_data[2]);		// y-axis      
	*z_value = ((temp_data[5] << 8) | temp_data[4]);		// z-axis       
 */
}
void acc_sensor_get_acch(uint16_t *x_value, uint16_t *y_value, uint16_t *z_value)
{	
	uint8_t temp_data[5];
    
    motion_sensor_read_multi_data(temp_data, MOTION_SENSOR_XOUTA_L, MOTION_SENSOR_ADDRESS, 1);
    motion_sensor_read_multi_data(&temp_data[1], MOTION_SENSOR_XOUTA_H, MOTION_SENSOR_ADDRESS, 1);
    
    
	*x_value = ((temp_data[1] << 8) | temp_data[0]);		// x-axis       
	 motion_sensor_read_multi_data(temp_data, MOTION_SENSOR_YOUTA_L, MOTION_SENSOR_ADDRESS, 1);
	 motion_sensor_read_multi_data(&temp_data[1], MOTION_SENSOR_YOUTA_H, MOTION_SENSOR_ADDRESS, 1);
	*y_value = ((temp_data[1] << 8) | temp_data[0]);		// x-axis       
     motion_sensor_read_multi_data(temp_data, MOTION_SENSOR_ZOUTA_L, MOTION_SENSOR_ADDRESS,1);
	 motion_sensor_read_multi_data(&temp_data[1], MOTION_SENSOR_ZOUTA_H, MOTION_SENSOR_ADDRESS, 1);
	*z_value = ((temp_data[1] << 8) | temp_data[0]);		// x-axis       
/*
	motion_sensor_read_multi_data(temp_data, MOTION_SENSOR_XOUT_L, MOTION_SENSOR_ADDRESS, 6);
	
	*x_value = ((temp_data[1] << 8) | temp_data[0]);		// x-axis       
	*y_value = ((temp_data[3] << 8) | temp_data[2]);		// y-axis      
	*z_value = ((temp_data[5] << 8) | temp_data[4]);		// z-axis       
 */
}
#endif



