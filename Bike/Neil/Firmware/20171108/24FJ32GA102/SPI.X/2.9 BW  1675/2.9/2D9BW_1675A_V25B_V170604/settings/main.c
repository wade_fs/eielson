//备注:用Source Insight软件浏览程序效果最佳
 
//	  
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx




//xx Includes xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#include "msp430x22x4.h"
#include "waveinit.h"
#include "image.h"



//xx Private macro xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#define SDA_H     	(P1OUT |=BIT7)						// P1.7
#define SDA_L     	(P1OUT &=~BIT7)
#define SCLK_H    	(P1OUT |=BIT6)  					// P1.6
#define SCLK_L   	(P1OUT &=~BIT6) 
#define nCS_H     	(P1OUT |=BIT5)						// P1.5
#define nCS_L     	(P1OUT &=~BIT5)
#define nDC_H     	(P1OUT |=BIT4)						// P1.4
#define nDC_L     	(P1OUT &=~BIT4)
#define nRST_H     	(P1OUT |=BIT3)						// P1.3 
#define nRST_L     	(P1OUT &=~BIT3)


#define DELAY_TIME	2									// 图片显示完停留时间(单位:秒)

#define MODE1  											// panel scan direction



#define PIC_BLACK		252
#define PIC_WHITE		255
#define PIC_A			1
#define PIC_B   	    2
#define PIC_HLINE		3
#define PIC_VLINE	    4
#define PIC_C			5
#define PIC_D   	    6
#define PIC_E		    7
#define PIC_F	        8
/*
#define PIC_SUNING		3
#define PIC_SHOUSI 		4
#define PIC_BAZHE 		5
#define PIC_CHESSBOARD		6
#define PIC_JIFEN		7
#define PIC_FU			8
#define PIC_GC4			9
#define PIC_SAVE		10
#define PIC_HK                  13
#define PIC_T                   14
#define PIC_P                   15
#define PIC_Z                   16
#define PIC_COOK                17
*/
//xx Private functions xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   延时函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void DELAY_100nS(int delaytime)   						// 30us 
{
	int i,j;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<1;j++);
}

void DELAY_mS(int delaytime)    						// 1ms
{
	int i,j;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<200;j++);
}

void DELAY_S(int delaytime)     						// 1s
{
	int i,j,k;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<1000;j++)
			for(k=0;k<200;k++);
}



//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   电子纸驱动操作函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// 复位 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void RESET()
{
	nRST_L;
	DELAY_mS(1);								
 	nRST_H;
  	DELAY_mS(1);
}

// 读忙 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void READBUSY()
{
  	while(1)
  	{
   		_NOP();
   	 	if((P1IN & 0x04)==0)
    		break;
  	}      
}

// 写命令 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void SPI4W_WRITECOM(unsigned char INIT_COM)
{
  	unsigned char TEMPCOM;
  	unsigned char scnt;

  	TEMPCOM=INIT_COM;
    	SCLK_H;  
 	nDC_L;
  	nCS_H;
  	nCS_L;
  	for(scnt=0;scnt<8;scnt++)
  	{
    	if(TEMPCOM&0x80)
     	 	SDA_H;
    	else
      		SDA_L;
    	SCLK_L;  
    	SCLK_H;  
    	TEMPCOM=TEMPCOM<<1;
  	}
  	nCS_H;  
}

// 写数据 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void SPI4W_WRITEDATA(unsigned char INIT_DATA)
{
  	unsigned char TEMPCOM;
  	unsigned char scnt;

  	TEMPCOM=INIT_DATA;
    	SCLK_H;  
 	nDC_H;
  	nCS_H;
  	nCS_L;
  	for(scnt=0;scnt<8;scnt++)
  	{
    	if(TEMPCOM&0x80)
     	 	SDA_H;
    	else
      		SDA_L;
    	SCLK_L;  
    	SCLK_H;  
    	TEMPCOM=TEMPCOM<<1;
  	}
  	nCS_H;  
}

// 写波形数据表 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void WRITE_LUT()
{
	unsigned char i;
	
	SPI4W_WRITECOM(0x32);			// write LUT register
	for(i=0;i<29;i++)			// write LUT register with 29bytes instead of 30bytes 2D13
        SPI4W_WRITEDATA(init_data[i]);
}

//void WriteFlow()
//{
//	unsigned char i;
//	
//	SPI4W_WRITECOM(0x24);		// write RAM
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data0[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data1[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data2[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data3[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data4[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data5[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data6[i]);
//	for(i=0;i<18;i++)	
//        SPI4W_WRITEDATA(init_data7[i]);
//}

// 电子纸驱动初始化 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void INIT_SSD1673()
{

	SPI4W_WRITECOM(0x01);		// Set MUX as 250
	SPI4W_WRITEDATA(0xF9);
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x3A);		// Set 50Hz
	SPI4W_WRITEDATA(0x06);
	SPI4W_WRITECOM(0x3B);		// Set 50Hz
	SPI4W_WRITEDATA(0x0B);
	
	SPI4W_WRITECOM(0x11);		// data enter mode
	SPI4W_WRITEDATA(0x01);
	SPI4W_WRITECOM(0x44);		// set RAM x address start/end, in page 36
	SPI4W_WRITEDATA(0x00);		// RAM x address start at 00h;
	SPI4W_WRITEDATA(0x0f);		// RAM x address end at 0fh(15+1)*8->128    2D13
	SPI4W_WRITECOM(0x45);		// set RAM y address start/end, in page 37
	SPI4W_WRITEDATA(0xF9);		// RAM y address start at FAh;		    2D13
	SPI4W_WRITEDATA(0x00);		// RAM  y address end at 00h;		    2D13
	
       	SPI4W_WRITECOM(0x2C);      // vcom
   	SPI4W_WRITEDATA(0x82);     //-2.5V
     	//SPI4W_WRITEDATA(0x69);     //-2V
//  	    SPI4W_WRITEDATA(0x4B);     //-1.4V
//   	SPI4W_WRITEDATA(0x50);     //-1.5V
//   	SPI4W_WRITEDATA(0x37);     //-1V
//   	SPI4W_WRITEDATA(0x1E);     //-0.5V

	SPI4W_WRITECOM(0x3C);		// board
	SPI4W_WRITEDATA(0x33);		//GS1-->GS1


	WRITE_LUT();
}

// 入深度睡眠 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void enterdeepsleep()
{
  	SPI4W_WRITECOM(0x10);
  	SPI4W_WRITEDATA(0x01);
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   图片显示函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void dis_img(unsigned char num)
{
	unsigned int row, col;
	unsigned int pcnt;
	    SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;
	    SPI4W_WRITEDATA(0x00);
	    SPI4W_WRITECOM(0x4F);		// set RAM y address count to 250;	2D13
	    SPI4W_WRITEDATA(0xF9);

	
	SPI4W_WRITECOM(0x24);
	
	pcnt = 0;											// 复位或保存提示字节序号
	for(col=0; col<252; col++)							// 总共172列		// send 128x252bits ram 2D13
	{
		for(row=0; row<16; row++)						// 总共72行，每个像素2bit,即 72/4 字节
		{
				switch (num)
				{
				case PIC_A:
					SPI4W_WRITEDATA(gImage_4589[pcnt]);
					break;	
                                        
                                        
                                  case PIC_B:
					SPI4W_WRITEDATA(gImage_ccr[pcnt]);
                                        break;
                                        
                                   case PIC_C:
					SPI4W_WRITEDATA(gImage_2d23[pcnt]);
					break;	     
                                        
                                   case PIC_D:
					SPI4W_WRITEDATA(gImage_2d24[pcnt]);
					break;	        

                case PIC_VLINE:
                    if(col>125)
					  SPI4W_WRITEDATA(0xff);
                    else
                      SPI4W_WRITEDATA(0x00);
					break;
                                        
                 case PIC_HLINE:
                    if(row>8)
					  SPI4W_WRITEDATA(0xff);
                    else if(row==8)
					  SPI4W_WRITEDATA(0x0f);
                    else
                      SPI4W_WRITEDATA(0x00);
					break;	
					
				case PIC_WHITE:
					SPI4W_WRITEDATA(0xff);
					break;	

				case PIC_BLACK:
					SPI4W_WRITEDATA(0x00);
					break;	
			   	default:
					break;
				}
			pcnt++;
		}
	}

	SPI4W_WRITECOM(0x22);
	SPI4W_WRITEDATA(0xC7);		//Load LUT from MCU(0x32), Display update
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
	

}

void Image_All_Black()
{
	unsigned char row, col;


	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;	2D3
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 250;	2D13
	SPI4W_WRITEDATA(0xF9);
	SPI4W_WRITECOM(0x24);
	for(col=0; col<252; col++)							// 总共172列		// send 128x252bits ram 2D13
	{
		for(row=0; row<16; row++)						// 总共72行，每个像素2bit,即 72/4 字节
		{
			SPI4W_WRITEDATA(0x00);
		}
	}
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
}

void Image_All_White()
{
	unsigned char row, col;

	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;	2D3
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 250;	2D13
	SPI4W_WRITEDATA(0xF9);
	SPI4W_WRITECOM(0x24);
	for(col=0; col<252; col++)							// 总共252列		// send 128x252bits ram 2D13
	{
		for(row=0; row<16; row++)						// 总共128行，每个像素1bit,即 128/8 字节
		{
			SPI4W_WRITEDATA(0xff);
		}
	}
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
}

void Image_All_Gray1()
{
	unsigned char row, col;

	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;	2D3
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 250;	2D13
	SPI4W_WRITEDATA(0xF9);
	SPI4W_WRITECOM(0x24);
	for(col=0; col<252; col++)							// 总共252列		// send 128x252bits ram 2D13
	{
		for(row=0; row<16; row++)						// 总共128行，每个像素1bit,即 128/8 字节
		{
			SPI4W_WRITEDATA(0x55);
		}
	}
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
}

void Image_All_Gray2()
{
	unsigned char row, col;

	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;	2D3
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 250;	2D13
	SPI4W_WRITEDATA(0xF9);
	SPI4W_WRITECOM(0x24);
	for(col=0; col<252; col++)							// 总共252列		// send 128x252bits ram 2D13
	{
		for(row=0; row<16; row++)						// 总共128行，每个像素1bit,即 128/8 字节
		{
			SPI4W_WRITEDATA(0xaa);
		}
	}
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
}

void Image_Source_Line()
{
	unsigned char row, col;

	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;	2D3
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 250;	2D13
	SPI4W_WRITEDATA(0xF9);
	SPI4W_WRITECOM(0x24);
	for(col=0; col<252; col++)							// 总共252列		// send 128x252bits ram 2D13
	{
		for(row=0; row<16; row++)						// 总共128行，每个像素1bit,即 128/8 字节
		{
			SPI4W_WRITEDATA(0xcc);
		}
	}
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
}

void Image_Gate_Line()
{
	unsigned char row, col;

	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;	2D3
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 250;	2D13
	SPI4W_WRITEDATA(0xF9);
	SPI4W_WRITECOM(0x24);
	for(col=0; col<252/2; col++)							// 总共252列		// send 128x252bits ram 2D13
	{
		for(row=0; row<16; row++)						// 总共128行，每个像素1bit,即 128/8 字节
		{
			SPI4W_WRITEDATA(0xff);
		}
		for(row=0; row<16; row++)						// 总共128行，每个像素1bit,即 128/8 字节
		{
			SPI4W_WRITEDATA(0x00);
		}
	}
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
}   





//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   主函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void main( void )
{
	//int i;
        

	WDTCTL = WDTPW + WDTHOLD;							// Stop watchdog timer to prevent time out reset
	BCSCTL1 = CALBC1_1MHZ; 								// set DCO frequency 1MHZ
	DCOCTL = CALDCO_1MHZ; 

	P1DIR |=0xF8;  										// set P1.3~7 output
	P3OUT = 0XFF;
        P3DIR = 0xff;										// p3.4,p3.5 设为输入

		
	RESET();											// 电子纸控制器复位
    	SPI4W_WRITECOM(0x12);			//SWRESET
	READBUSY();
	
	
	INIT_SSD1673();	
    	SPI4W_WRITECOM(0x21);								//
    	SPI4W_WRITEDATA(0x83);
    	dis_img(PIC_WHITE);

    	SPI4W_WRITECOM(0x21);								//
    	SPI4W_WRITEDATA(0x03);

	//while(1)
	//{
            	dis_img(PIC_BLACK);
                DELAY_S(DELAY_TIME);
      	 //       dis_img(PIC_WHITE);
		//DELAY_S(DELAY_TIME);
      	dis_img(PIC_VLINE);
		DELAY_S(DELAY_TIME);
	dis_img(PIC_HLINE);
		DELAY_S(DELAY_TIME);
	//	dis_img(PIC_A);
	//	DELAY_S(DELAY_TIME);
                        
        //     dis_img(PIC_BLACK);
	 //    DELAY_S(DELAY_TIME);
      	 //    dis_img(PIC_WHITE);
	 //    DELAY_S(DELAY_TIME);
                        
        //        dis_img(PIC_B);
	//	DELAY_S(DELAY_TIME);
                
         //    dis_img(PIC_BLACK);
	 //    DELAY_S(DELAY_TIME);
      	 //    dis_img(PIC_WHITE);
	 //    DELAY_S(DELAY_TIME);
                        
         //       dis_img(PIC_C);
	//	DELAY_S(DELAY_TIME);
             
          //   dis_img(PIC_BLACK);
	  //   DELAY_S(DELAY_TIME);
      	     dis_img(PIC_WHITE);
	   DELAY_S(DELAY_TIME);
                        
                dis_img(PIC_D);
		DELAY_S(DELAY_TIME);   
                dis_img(PIC_BLACK);
                DELAY_S(DELAY_TIME);
                        
	//}

}
