package com.mapzen.speakerbox.example

import com.mapzen.speakerbox.Speakerbox

import android.os.Handler
import android.speech.tts.TextToSpeech
import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import android.widget.ToggleButton

import java.util.Locale

/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : Fragment() {

    internal lateinit var speakerbox: Speakerbox
    internal lateinit var languagesLayout: LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        val textView = view.findViewById<View>(R.id.text) as EditText
        speakerbox = Speakerbox(activity!!.application)
        speakerbox.setActivity(activity!!)

        // Test calling play() immediately (before TTS initialization is complete).
        speakerbox.play(textView.text)

        val speakButton = view.findViewById<View>(R.id.speak) as Button
        speakButton.setOnClickListener { speakerbox.play(textView.text) }

        val speakStatus = view.findViewById<View>(R.id.speak_status) as TextView

        val speakWNotifyButton = view.findViewById<View>(R.id.speak_w_notify) as Button
        speakWNotifyButton.setOnClickListener {
            val onStart = Runnable { speakStatus.text = getText(R.string.start_speaking) }
            val onDone = Runnable {
                speakStatus.text = getText(R.string.done_speaking)
                clearStatusText(speakStatus)
            }
            val onError = Runnable {
                speakStatus.text = getText(R.string.error_speaking)
                clearStatusText(speakStatus)
            }
            speakerbox.play(textView.text.toString(), onStart, onDone, null)
        }

        val stopButton = view.findViewById<View>(R.id.stop) as Button
        stopButton.setOnClickListener { speakerbox.stop() }

        val muteButton = view.findViewById<View>(R.id.mute) as ToggleButton
        muteButton.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                speakerbox.mute()
            } else {
                speakerbox.unmute()
            }
        }

        val add = view.findViewById<View>(R.id.queue_mode_add) as RadioButton
        add.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                speakerbox.setQueueMode(TextToSpeech.QUEUE_ADD)
            }
        }

        val flush = view.findViewById<View>(R.id.queue_mode_flush) as RadioButton
        flush.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                speakerbox.setQueueMode(TextToSpeech.QUEUE_FLUSH)
            }
        }

        val requestButton = view.findViewById<View>(R.id.request_focus_btn) as Button
        requestButton.setOnClickListener { speakerbox.requestAudioFocus() }

        val abandonButton = view.findViewById<View>(R.id.abandon_focus_btn) as Button
        abandonButton.setOnClickListener { speakerbox.abandonAudioFocus() }

        val getLanguagesBtn = view.findViewById<View>(R.id.get_languages_btn) as Button
        getLanguagesBtn.setOnClickListener { getAvailableLanguages() }

        languagesLayout = view.findViewById<View>(R.id.available_languages) as LinearLayout

        return view
    }

    private fun clearStatusText(speakStatus: TextView) {
        Handler().postDelayed({ speakStatus.text = "" }, 1000)
    }

    private fun getAvailableLanguages() {
        val availableLanguages = speakerbox.availableLanguages

        languagesLayout.removeAllViews()

        val inflater = LayoutInflater.from(this.activity)
        for (locale in availableLanguages) {

            val view = inflater.inflate(R.layout.language_row, null, false)
            val textView = view.findViewById<View>(R.id.text_view) as TextView
            textView.text = locale.displayName

            val button = view.findViewById<View>(R.id.btn) as Button
            button.setOnClickListener { speakerbox.setLanguage(locale) }

            languagesLayout.addView(view)
        }
    }
}
