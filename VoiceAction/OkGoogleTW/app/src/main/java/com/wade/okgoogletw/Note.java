package com.wade.okgoogletw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

public class Note extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        note(getIntent());
    }

    private void note(Intent intent) {
        String s = intent.getStringExtra(Intent.EXTRA_TEXT);
        TextView textView = findViewById(R.id.tvNote);

        textView.setText("日記: " + s+" @ "+ DateFormat.getDateTimeInstance().format(new Date()));
    }
}
