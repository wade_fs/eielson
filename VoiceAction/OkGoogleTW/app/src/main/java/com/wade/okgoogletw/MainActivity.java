package com.wade.okgoogletw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        main(getIntent());
    }

    private void main(Intent intent) {
        Intent t = getIntent();
        String s = t.getStringExtra(Intent.EXTRA_TEXT);
        TextView textView = findViewById(R.id.tvMain);
        if (s != null) {
            textView.setText("main: " + s);
        } else {
            textView.setText("不用特意點開本應用，直接回到 Home，透過 Ok google 來測試\n"+
            "take a note / 寫下 / 記錄\n"+
            "take a selfie / 拍照\n"+
            "play / 播 / 播放\n"+
            "open / 搜尋 / 打開");
        }
    }
}
