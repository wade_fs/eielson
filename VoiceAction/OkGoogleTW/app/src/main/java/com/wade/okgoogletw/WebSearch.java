package com.wade.okgoogletw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

public class WebSearch extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        websearch(getIntent());
    }

    private void websearch(Intent intent) {
        Intent t = getIntent();
        String s = t.getStringExtra(Intent.EXTRA_TEXT);
        TextView textView = findViewById(R.id.tvWebSearch);
        textView.setText("WebSearch: " + s+" @ "+ DateFormat.getDateTimeInstance().format(new Date()));
    }
}
