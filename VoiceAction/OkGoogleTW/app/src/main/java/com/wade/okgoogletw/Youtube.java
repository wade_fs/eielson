package com.wade.okgoogletw;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

public class Youtube extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        youtube(getIntent());
    }

    private void youtube(Intent intent) {
        String s = intent.getStringExtra(SearchManager.QUERY);
        TextView textView = findViewById(R.id.tvYoutube);

        textView.setText("播放: " + s+" @ "+ DateFormat.getDateTimeInstance().format(new Date()));
    }
}
