package com.wade.okgoogletw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

public class TakePicture extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_takepicture);
        takePicture(getIntent());
    }

    private void takePicture(Intent intent) {
        if (intent == null) finish();
        String s = intent.getStringExtra(Intent.EXTRA_TEXT);
        TextView textView = findViewById(R.id.tvTakepicture);

        textView.setText("拍照: " + " @ "+ DateFormat.getDateTimeInstance().format(new Date()));
    }
}
