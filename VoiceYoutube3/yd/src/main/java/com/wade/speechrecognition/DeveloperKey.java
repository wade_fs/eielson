// Copyright 2014 Google Inc. All Rights Reserved.

package com.wade.speechrecognition;

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
public class DeveloperKey {

  /**
   * Please replace this with a valid API key which is enabled for the
   * YouTube Data API v3 service. Go to the
   * <a href="https://console.developers.google.com/">Google Developers Console</a>
   * to register a new developer key.
   */
  public static final String DEVELOPER_KEY = "AIzaSyAZ3BV9h9yQIK_9wkwcQj_aO9R9fEI4NZw";  // "AIzaSyDcggEJYxMCWTn-MOAHROQ63m-558aQd-U";

}
