package com.wade.speechrecognition;

import android.content.Context;
import android.util.Log;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class YoutubeConnector {
    private YouTube youtube;
    private YouTube.Search.List query;

    public YoutubeConnector(Context context) {
//        Log.d("MyLog", "new YoutubeConnector");
        youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {
            }
        }).setApplicationName(context.getString(R.string.app_name)).build();

        try {
            query = youtube.search().list("id,snippet");
            query.setKey(DeveloperKey.DEVELOPER_KEY);
            query.setType("video");
            query.setFields("items(id/videoId,snippet/title,snippet/description,snippet/thumbnails/default/url)");
        } catch (IOException e) {
            Log.d("MyLog", "Could not initialize: " + e.getMessage());
        }
    }

    public List<VideoItem> search(String keywords) {
//        Log.d("MyLog", "yc.search('"+keywords+"')");
        List<VideoItem> items = new ArrayList<VideoItem>();

        query.setQ(keywords);
        try {
            SearchListResponse response = query.execute();
            List<SearchResult> results = response.getItems();

            for (SearchResult result : results) {
                VideoItem item = new VideoItem();
                item.setTitle(result.getSnippet().getTitle());
                item.setDescription(result.getSnippet().getDescription());
                item.setThumbnailURL(result.getSnippet().getThumbnails().getDefault().getUrl());
                item.setId(result.getId().getVideoId());
                items.add(item);
            }
        } catch (IOException e) {
            Log.d("MyLog", "Exception @ search("+keywords+") :"+e.getMessage());
        }

        return items;
    }

    public List<VideoItem> searchByVideoIds(List<String> ids) {
//        Log.d("MyLog", "yc.searchByVideoIds('"+ ids.toArray().toString()+"')");
        List<VideoItem> items = new ArrayList<VideoItem>();

        for (String keyword : ids) {
//            Log.d("MyLog", "searchByVideoIds() for '"+keyword+"'");
            query.setQ(keyword);
            try {
                SearchListResponse response = query.execute();
                List<SearchResult> results = response.getItems();

                for (SearchResult result : results) {
                    if (result.getId().getVideoId().equals(keyword)) {
                        VideoItem item = new VideoItem();
                        item.setTitle(result.getSnippet().getTitle());
                        item.setDescription(result.getSnippet().getDescription());
                        item.setThumbnailURL(result.getSnippet().getThumbnails().getDefault().getUrl());
                        item.setId(result.getId().getVideoId());
                        items.add(item);
                        break;
                    }
                }
            } catch (IOException e) {
                Log.d("MyLog", "Exception @ search(" + keyword + ") :" + e.getMessage());
            }
        }

        return items;
    }
}

