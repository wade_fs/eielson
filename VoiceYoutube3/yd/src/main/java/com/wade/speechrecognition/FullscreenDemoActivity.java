/*
 * Copyright 2012 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wade.speechrecognition;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

/**
 * Sample activity showing how to properly enable custom fullscreen behavior.
 * <p>
 * This is the preferred way of handling fullscreen because the default fullscreen implementation
 * will cause re-buffering of the video.
 */
public class FullscreenDemoActivity extends YouTubeFailureRecoveryActivity implements
        YouTubePlayer.OnFullscreenListener {

    private static final int PORTRAIT_ORIENTATION = Build.VERSION.SDK_INT < 9
            ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            : ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;

    private LinearLayout baseLayout;
    private YouTubePlayerView playerView;
    private YouTubePlayer player;
    private boolean isPlaylist = false;
    private static boolean isFinished = false;

    private MyPlaybackEventListener playbackEventListener;
    private boolean fullscreen;
    private static FullscreenDemoActivity me;
    static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 100 && isFinished) me.finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.fullscreen_demo);
        baseLayout = (LinearLayout) findViewById(R.id.layout);
        playerView = (YouTubePlayerView) findViewById(R.id.player);
        playerView.initialize(DeveloperKey.DEVELOPER_KEY, this);
        playbackEventListener = new MyPlaybackEventListener();

        doLayout();
        me = this;
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer,
                                        boolean wasRestored) {
        player = youTubePlayer;
        player.setPlaybackEventListener(playbackEventListener);
        player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
        player.setOnFullscreenListener(this);

        boolean autoPlay = false;
        autoPlay = getIntent().getBooleanExtra("autoPlay", false);
        String vid = getIntent().getStringExtra("VIDEO_ID");
        String pid = getIntent().getStringExtra("PLAYLIST_ID");

        if (vid != null) {
            if (!wasRestored) {
                isPlaylist = false;
                if (autoPlay)
                    player.loadVideo(vid);
                else
                    player.cueVideo(vid);
            }
        } else if (pid != null) {
            if (!wasRestored) {
                isPlaylist = true;
                if (autoPlay)
                    player.loadPlaylist(pid);
                else
                    player.cuePlaylist(pid);
            }
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFullscreen(boolean isFullscreen) {
        doLayout();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        doLayout();
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return playerView;
    }

    private void doLayout() {
        LinearLayout.LayoutParams playerParams =
                (LinearLayout.LayoutParams) playerView.getLayoutParams();
        // When in fullscreen, the visibility of all other views than the player should be set to
        // GONE and the player should be laid out across the whole screen.
        playerParams.width = LayoutParams.MATCH_PARENT;
        playerParams.height = LayoutParams.MATCH_PARENT;
    }

    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {
        String playbackState = "NOT_PLAYING";
        String bufferingState = "NO_BUFFER";

        @Override
        public void onPlaying() {
            playbackState = "PLAYING";
//            Log.d("MyLog", "PlaybackState: " + playbackState);
        }

        @Override
        public void onBuffering(boolean isBuffering) {
            bufferingState = isBuffering ? "(BUFFERING)" : "(FULL)";
//            Log.d("MyLog", "BufferingState: " + bufferingState);
            isFinished = false;
        }

        @Override
        public void onStopped() {
//            Log.d("MyLog", "STOP: PlaybackState: " + playbackState +", BufferState: "+bufferingState);
            if (!bufferingState.equals("NO_BUFFER")) {
                if (!(isPlaylist && player.hasNext())) {
                    finish();
                }
            } else if (playbackState.equals("NOT_PLAYING")) {
                isFinished = true;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Message msg = Message.obtain();
                        msg.what = 100;
                        mHandler.sendMessageDelayed(msg, 3000);
                    }
                }).start();
            }
            playbackState = "STOPPED";
        }

        @Override
        public void onPaused() {
            playbackState = "PAUSED";
//            Log.d("MyLog", "PlaybackState: " + playbackState);
            finish();
        }

        private String formatTime(int millis) {
            int seconds = millis / 1000;
            int minutes = seconds / 60;
            int hours = minutes / 60;

            return (hours == 0 ? "" : hours + ":")
                    + String.format("%02d:%02d", minutes % 60, seconds % 60);
        }

        @Override
        public void onSeekTo(int endPositionMillis) {
//            Log.d("MyLog", String.format("\tSEEKTO: (%s/%s)",
//                    formatTime(endPositionMillis),
//                    formatTime(player.getDurationMillis())));
        }
    }
}
