package com.wade.voiceyoutube;

import android.util.Log;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by wade on 2018/2/8.
 */

public class Command {
    ArrayList<Map.Entry<String,String>> sameWords;
    public ArrayList<String> cmdsPlay = new ArrayList<String>() {
        {
            add("play");
            add("播放");
        }};
    public ArrayList<String> cmdsNext = new ArrayList<String>() {
        {
            add("next");
            add("下一首");
        }};
    public ArrayList<String> cmdsBack = new ArrayList<String>() {
        {
            add("back");
            add("上一首");
        }};
    public ArrayList<String> cmdsPlaylist = new ArrayList<String>() {
        {
            add("add");
            add("加入");
        }};
    public ArrayList<String> cmdsSave = new ArrayList<String>() {
        {
            add("save");
            add("儲存");
        }};
    public ArrayList<String> cmdsClean = new ArrayList<String>() {
        {
            add("clean");
            add("清空");
            add("清除");
        }};
    public ArrayList<String> cmdsRepeat = new ArrayList<String>() {
        {
            add("repeat");
            add("重覆");
        }};
    public ArrayList<String> cmdsMyFavorites = new ArrayList<String>() {
        {
            add("my favorite");
            add("我的最愛");
        }};

    public Command () {
        initSameWords();
    }

    private void initSameWords() {
        sameWords = new ArrayList<>();
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "剝"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "菠"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "波"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "撥"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "玻"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "薄"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "柏"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "檗"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "擘"));
        sameWords.add(new AbstractMap.SimpleEntry<>("播", "簸"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "廈"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "嚇"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "唬"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "匣"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "陝"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "俠"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "葭"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "狎"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "狹"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "峽"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "轄"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "暇"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "呷"));
        sameWords.add(new AbstractMap.SimpleEntry<>("下", "蝦"));
        sameWords.add(new AbstractMap.SimpleEntry<>("上", "尚"));
        sameWords.add(new AbstractMap.SimpleEntry<>("上", "喪"));
        sameWords.add(new AbstractMap.SimpleEntry<>("上", "桑"));
        sameWords.add(new AbstractMap.SimpleEntry<>("上", "賞"));
        sameWords.add(new AbstractMap.SimpleEntry<>("上", "晌"));
        sameWords.add(new AbstractMap.SimpleEntry<>("上", "嗓"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "醫"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "伊"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "依"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "壹"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "揖"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "椅"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "依"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "義"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "藝"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "役"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "佚"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "亦"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "譯"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "億"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "詣"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "儀"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "宜"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "誼"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "詣"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "詒"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "遺"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "夷"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "姨"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "疑"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "夷"));
        sameWords.add(new AbstractMap.SimpleEntry<>("一", "夷"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "守"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "手"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "獸"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "受"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "售"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "壽"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "授"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "瘦"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "緩"));
        sameWords.add(new AbstractMap.SimpleEntry<>("首", "叟"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "副"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "附"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "富"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "福"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "付"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "傅"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "訃"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "駙"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "婦"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "負"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "縛"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "賦"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "父"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "府"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "俯"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "嘸"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "拊"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "撫"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "輔"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "腐"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "釜"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "夫"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "苻"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "佛"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "弗"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "幅"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "伏"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "扶"));
        sameWords.add(new AbstractMap.SimpleEntry<>("覆", "拂"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "輕"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "青"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "卿"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "傾"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "氫"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "蜻"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "情"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "晴"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "擎"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "黥"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "慶"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "磬"));
        sameWords.add(new AbstractMap.SimpleEntry<>("清", "頃"));
        sameWords.add(new AbstractMap.SimpleEntry<>("空", "控"));
        sameWords.add(new AbstractMap.SimpleEntry<>("空", "恐"));
        sameWords.add(new AbstractMap.SimpleEntry<>("空", "孔"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "楚"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "廚"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "鋤"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "芻"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "蜍"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "躕"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "雛"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "儲"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "杵"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "楮"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "褚"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "礎"));
        sameWords.add(new AbstractMap.SimpleEntry<>("除", "處"));
        sameWords.add(new AbstractMap.SimpleEntry<>("最", "醉"));
        sameWords.add(new AbstractMap.SimpleEntry<>("最", "罪"));
        sameWords.add(new AbstractMap.SimpleEntry<>("最", "綴"));
        sameWords.add(new AbstractMap.SimpleEntry<>("最", "墜"));
        sameWords.add(new AbstractMap.SimpleEntry<>("最", "惴"));
        sameWords.add(new AbstractMap.SimpleEntry<>("最", "縋"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "艾"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "唚"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "噫"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "嬡"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "礙"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "曖"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "璦"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "哀"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "挨"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "矮"));
        sameWords.add(new AbstractMap.SimpleEntry<>("愛", "欸"));
    }

    // cmd 夏宜手, str 下一首
    private boolean fuzzyComp(String cmd, String str) {
        for (int i=0; i<str.length(); ++i) {
            String src = cmd;
            for (Map.Entry<String,String> pair : sameWords) {
                if (src.substring(i, i+1).equals(pair.getValue())) {
                    if (i>=src.length()-1)
                        src = src.substring(0,i) + pair.getKey();
                    else
                        src = src.substring(0,i) + pair.getKey()+src.substring(i+1);
                }
            }
            if (src.equals(str)) return true;
        }
        return false;
    }

    // 傳回去主要用途是切割字串，所以長度比較重要, cmd 是語音辨識結果，需要修正
    public String matchCmd0(String cmd, ArrayList<String> cmds) {
        for (String str:cmds) {
            // 辨識到的字比命令短，忽略
            if (cmd.length() < str.length()) continue;
            // 先完整比對
            if (cmd.startsWith(str)) return str;
            // 如果有英文，那麼一定是錯的
            if (str.matches("[a-z]")) continue;
            // 此時需要模糊比對 cmd 的前 str.len 長度
            if (fuzzyComp(cmd, str)) return str;
        }
        return "";
    }

    public String keyword;
    public int matchCmd(String cmd) {
//        Log.d("MyLog", "matchCmd('"+cmd+"')");
        String key;
        // 先比對 播放
        String matched;
        if (!(matched = matchCmd0(cmd, cmdsPlay)).isEmpty()) {
            key = cmd.substring(matched.length());
            if (key.isEmpty()) {
                return Activity_DroidSpeech.CmdPlay;
            } else {
                keyword = key; // +"官方MV";
                return Activity_DroidSpeech.CmdPlayWithKey;
            }
        } else if (!matchCmd0(cmd, cmdsNext).isEmpty()) {
            return Activity_DroidSpeech.CmdNext;
        } else if (!matchCmd0(cmd, cmdsBack).isEmpty()) {
            return Activity_DroidSpeech.CmdBack;
        } else if (!matchCmd0(cmd, cmdsMyFavorites).isEmpty()) {
            return Activity_DroidSpeech.CmdMyFavorites;
        } else if (!matchCmd0(cmd, cmdsPlaylist).isEmpty()) {
            return Activity_DroidSpeech.CmdPlayList;
        } else if (!matchCmd0(cmd, cmdsClean).isEmpty()) {
            return Activity_DroidSpeech.CmdClean;
        }
        return Activity_DroidSpeech.CmdUnknown;
    }
}
