package com.wade.voiceyoutube;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wade.droidspeech.DroidSpeech;
import com.wade.droidspeech.OnDSListener;
import com.wade.droidspeech.OnDSPermissionsListener;
import com.wade.speechrecognition.FullscreenDemoActivity;
import com.wade.speechrecognition.VideoItem;
import com.wade.speechrecognition.YoutubeConnector;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Droid Speech Example Activity
 *
 * @author Vikram Ezhil
 */

public class Activity_DroidSpeech extends Activity implements
        OnDSListener, OnDSPermissionsListener // 語音
{
    public final String TAG = "MyLog";

    private DroidSpeech droidSpeech;
    private EditText finalSpeechResult;

    /* 底下是給 Youtube API */
    private ListView videosFound;
    private Handler handler;
    private List<VideoItem> searchResults;
    // https://www.youtube.com/watch?v=kfXdP7nZIiE&list=PLFgquLnL59amN9tYr7o2a60yFUfzQO3sU
    String keyword = "請說出「播放XXX」"; // "卡拉ok歌曲"; // 要改成 channel for default: channel id: UC-9-kyTW8ZkZNDHQJ6FgpwQ
    String startPlayList = "PLAvnVYGnc8FTvyDIlY5i5Icb7oQY8-Skr";
    boolean hadSearched = false;
    Context context;
    int curPos = 0;
    ArrayAdapter<VideoItem> mAdapter;
    MyFavorites myFavorites;
    Boolean isMyFavorites = false;
    ArrayList<String> curIds;
    Command command;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Setting the layout;[.
        setContentView(R.layout.activity_droid_speech);
        context = this;

        // Initializing the droid speech and setting the listener
        droidSpeech = new DroidSpeech(this, getFragmentManager());
        droidSpeech.setOnDroidSpeechListener(this);
        droidSpeech.setShowRecognitionProgressView(false);
//        droidSpeech.setContinuousSpeechRecognition(true);
//        droidSpeech.setOfflineSpeechRecognition(true);
        droidSpeech.setTAG(TAG);

        finalSpeechResult = (EditText) findViewById(R.id.finalSpeechResult);

        /* 底下加入 Youtube API */
        videosFound = (ListView) findViewById(R.id.videos_found);
        addClickListener(); // 以上兩個元件的 onClick()

        finalSpeechResult.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_SEND) {
                    keyword = v.getText().toString();
                    Log.d("MyLog", "GO searchOnYoutube(" + keyword + ")");
                    searchOnYoutube(false);
                    return false;
                }
                return true;
            }
        });
        command = new Command();
        // for searching from youtube
        handler = new Handler();
        myFavorites = new MyFavorites(getApplicationContext());
        curIds = new ArrayList<>();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (videosFound.getAdapter() != null && videosFound.getAdapter().getCount() > 0)
            videosFound.setSelection(curPos);
        Log.d("MyLog", "onResume() "+keyword+","+hadSearched+","+isMyFavorites);
        if (!hadSearched) {
            finalSpeechResult.setText(keyword);
            loadMyFavorites();
            hadSearched = true;
            isMyFavorites = true;
            playPlaylistById(startPlayList); // 播放『歡迎畫面』
        }
        start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

        //儲存UI狀態到bundle中
        outState.putString("keyword", keyword);
        outState.putBoolean("hadSearched", hadSearched);
        outState.putStringArrayList("curIds", curIds);
        outState.putBoolean("isMyFavorites", isMyFavorites);
        Log.d("MyLog", "onSaveInstanceState() "+keyword+","+hadSearched+","+isMyFavorites);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        if (outState != null) {
            keyword = outState.getString("keyword");
            hadSearched = outState.getBoolean("hadSearched");
            curIds = outState.getStringArrayList("curIds");
            isMyFavorites = outState.getBoolean("isMyFavorites");
            Log.d("MyLog", "onRestoreInstanceState() "+keyword+","+hadSearched+","+isMyFavorites);
        }
    }

    @Override
    public void onDroidSpeechSupportedLanguages(String currentSpeechLanguage, List<String> supportedSpeechLanguages) {
//        Log.i(TAG, "Current speech language = " + currentSpeechLanguage);
//        Log.i(TAG, "Supported speech languages = " + supportedSpeechLanguages.toString());

        if (supportedSpeechLanguages.contains("cmn-")) {
            // Setting the droid speech preferred language as tamil if found
            droidSpeech.setPreferredLanguage("cmn-Hant-TW");
        }
    }

    @Override
    public void onDroidSpeechRmsChanged(float rmsChangedValue) {
    }

    @Override
    public void onDroidSpeechLiveResult(String liveSpeechResult) {
//        Log.i(TAG, "onDroidSpeechLiveResult = " + liveSpeechResult);
    }
    public static final int CmdUnknown     = 0;
    public static final int CmdPlay        = 1;
    public static final int CmdPlayWithKey = 2;
    public static final int CmdNext        = 3;
    public static final int CmdBack        = 4;
    public static final int CmdMyFavorites = 5;
    public static final int CmdPlayList    = 6;
    public static final int CmdClean       = 7;

    @Override
    public void onDroidSpeechFinalResult(String finalSpeechResult) {
//        Log.i(TAG, "onDroidSpeechFinalResult = " + finalSpeechResult);

        int cmd;
        switch (cmd = command.matchCmd(finalSpeechResult)) {
            case CmdPlay:
                performClickAt(curPos);
                break;
            case CmdPlayWithKey:
                keyword = command.keyword; // +"官方MV";
                this.finalSpeechResult.setText(keyword);
                searchOnYoutube(true);
                break;
            case CmdNext:
                if (curPos < videosFound.getAdapter().getCount() - 1) {
                    ++curPos;
                }
                performClickAt(curPos);
                break;
            case CmdBack:
                if (curPos > 0) {
                    --curPos;
                }
                performClickAt(curPos);
                break;
            case CmdMyFavorites:
                isMyFavorites = true;
                loadMyFavorites();
                break;
            case CmdPlayList:
                myFavorites.add(curIds.get(curPos));
                isMyFavorites = true;
                loadMyFavorites();
                break;
            case CmdClean:
                myFavorites.clean();
                keyword = "請說出「播放XXX」";
                isMyFavorites = false;
                loadMyFavorites();
                break;
            default: // CmdUnknown
        }
    }

    @Override
    public void onDroidSpeechClosedByUser() {
//        Log.d(TAG, "onDroidSpeechClosedByUser = " + finalSpeechResult);
    }

    @Override
    public void onDroidSpeechError(String errorMsg) {
        Log.i(TAG, "onDroidSpeechError = " + finalSpeechResult);
        Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
        stop();
    }

    // MARK: DroidSpeechPermissionsListener Method

    @Override
    public void onDroidSpeechAudioPermissionStatus(boolean audioPermissionGiven, String errorMsgIfAny) {
        Log.i(TAG, "onDroidSpeechAudioPermissionStatus = " + errorMsgIfAny);
        if (audioPermissionGiven) {
            start();
        } else {
            if (errorMsgIfAny != null) {
                Toast.makeText(this, errorMsgIfAny, Toast.LENGTH_LONG).show();
            }
            stop();
        }
    }

    ///////////////// VoiceRecognition /////////////////////
    public void start() {
//        Log.i(TAG, "start");
        droidSpeech.startDroidSpeechRecognition();
    }

    public void stop() {
//        Log.i(TAG, "stop");
        droidSpeech.closeDroidSpeechOperations();
    }

    /////////////// Youtube API ///////////////////
    private void addClickListener() {
        finalSpeechResult.setOnClickListener(view -> {});

        videosFound.setOnItemClickListener((parent, view, position, id) -> {
            playVideoById(searchResults.get(position).getId());
        });
    }

    private void playVideoById(String id) {
        Intent intent = new Intent(getApplication(), FullscreenDemoActivity.class);
        intent.putExtra("autoPlay", true);
        intent.putExtra("VIDEO_ID", id);
        droidSpeech.muteAudio(false);
        startActivity(intent);
    }

    private void playPlaylistById(String id) {
        Intent intent = new Intent(getApplication(), FullscreenDemoActivity.class);
        intent.putExtra("autoPlay", true);
        intent.putExtra("PLAYLIST_ID", id);
        droidSpeech.muteAudio(false);
        startActivity(intent);
    }

    private void searchOnYoutube(boolean autoPlay) {
//        Log.d("MyLog", "searchOnYoutube(" + keyword + ", " + autoPlay + ")");
        new Thread() {
            @Override
            public void run() {
                YoutubeConnector yc = new YoutubeConnector(Activity_DroidSpeech.this);
                searchResults = yc.search(keyword);
                handler.post(() -> updateVideosFound(autoPlay));
            }
        }.start();
    }

    private void performClickAt(int pos) {
        if (pos < videosFound.getAdapter().getCount()) {
            videosFound.setSelection(pos);
            playVideoById(searchResults.get(pos).getId());
        }
    }

    private void updateVideosFound(boolean autoPlay) {
        hadSearched = true;
        curIds = new ArrayList<>();
        for (VideoItem item : searchResults) {
            curIds.add(item.getId());
        }
        mAdapter = new ArrayAdapter<VideoItem>(getApplicationContext(), R.layout.video_item, searchResults) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.video_item, parent, false);
                }

                ImageView thumbnail = convertView.findViewById(R.id.video_thumbnail);
                TextView title = convertView.findViewById(R.id.video_title);
                TextView description = convertView.findViewById(R.id.video_description);

                VideoItem searchResult = searchResults.get(position);

                Picasso.with(getApplicationContext()).load(searchResult.getThumbnailURL()).into(thumbnail);
                title.setText(searchResult.getDescription());
                description.setText(searchResult.getDescription());
                return convertView;
            }
        };
        videosFound.setAdapter(mAdapter);
        if (!autoPlay) {
            curPos = 0;
            performClickAt(curPos);
        }
    }

    private void loadMyFavorites() {
        new Thread() {
            @Override
            public void run() {
                YoutubeConnector yc = new YoutubeConnector(Activity_DroidSpeech.this);
                searchResults = yc.searchByVideoIds(myFavorites.get());
                handler.post(() -> updateVideosFound(false));
            }
        }.start();
    }
}
