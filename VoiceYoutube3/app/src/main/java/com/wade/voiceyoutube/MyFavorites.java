package com.wade.voiceyoutube;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wade on 2018/2/7.
 */

public class MyFavorites extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "myfavorites.db";
    private static final int DATABASE_VERSION = 1;
    private static final String ID="id";
    private static final String URL="url";
    private static final String TABLE = "list";
    private SQLiteDatabase db = null;

    public MyFavorites(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public List<String> get() {
        List<String> myFavorites = new ArrayList<>();
        if (db == null) db = getWritableDatabase();
        if (db == null) return null;

        Cursor cursor = db.rawQuery("select * from "+TABLE, null);
        while (cursor.moveToNext()) {
            String url = cursor.getString(cursor.getColumnIndex(URL));
            Log.d("MyLog", "myFavorites, load URL='"+url+"'");
            myFavorites.add(url);
        }
        return myFavorites;
    }

    private boolean isIn(String str) {
        if (db == null) db = getWritableDatabase();
        str = str.replace(" ", "%20")
                .replace("/", "%2F")
                .replace("(", "%28")
                .replace(")", "%29");
        Cursor cursor = db.rawQuery("select * from list where url=\""+str+"\"", null);
        return cursor.moveToNext();
    }

    public void add(String item) {
        if (isIn(item)) return;
        if (db == null) db = getWritableDatabase();
        String[] columns = {ID, URL};
        Log.d("MyLog", "myFavorites, add URL="+item);
        ContentValues cv = new ContentValues();
        cv.put("url", item);
        db.insert("list", null, cv);
    }

    public void clean() {
        if (db == null) db = getWritableDatabase();
        db.execSQL("delete from list");
    }
}
