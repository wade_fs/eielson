package com.wade.voiceyoutube;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by wade on 2018/2/9.
 */

public class OnBoot extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            Intent mainIntent = new Intent(context, Activity_DroidSpeech.class);
            context.startActivity(mainIntent);
        }
    }
}
